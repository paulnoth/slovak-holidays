const holidays = require('./util/holidays');
const getEasterSunday = require('./util/getEasterSunday');
const getEasterMonday = require('./util/getEasterMonday');
const getGoodFriday = require('./util/getGoodFriday');
const {getGoodFridayName, getEasterSundayName, getEasterMondayName} = require('./util/easterNames');

const getAllHolidays = year => {
    if (!year || typeof year !== 'number' || year < 1993) {
        return [];
    }
    const fixedHolidays = holidays.filter(hol => year >= hol.validFrom && year <= hol.validTo)
        .map(hol => ({day: hol.day, month: hol.month, year, name: hol.name}));
    const easterSunday = getEasterSunday(year);
    const goodFriday = getGoodFriday(easterSunday);
    const easterMonday = getEasterMonday(easterSunday);

    return [
        ...fixedHolidays.slice(0, 2),
        {...goodFriday, name: getGoodFridayName()},
        {...easterSunday, name: getEasterSundayName()},
        {...easterMonday, name: getEasterMondayName()},
        ...fixedHolidays.slice(2),
    ];
}

module.exports = getAllHolidays;
