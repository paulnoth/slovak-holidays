const parseDate = date => {
    if (date instanceof Date) {
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        return {year, month, day};
    }
    return {year: date.year, month: date.month, day: date.day};
}

module.exports = parseDate;
