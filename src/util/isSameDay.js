const isSameDay = (holiday, day) => holiday.day === day;

module.exports = isSameDay;
