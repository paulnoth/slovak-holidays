const getGoodFridayName = (locale = 'sk') => {
    if (locale === 'en') {
        return 'Good Friday';
    }
    if (locale === 'de') {
        return 'Karfreitag';
    }
    return 'Veľký piatok';
}

const getEasterSundayName = (locale = 'sk') => {
    if (locale === 'en') {
        return 'Easter Day';
    }
    if (locale === 'de') {
        return 'Ostersonntag';
    }
    return 'Veľkonočná nedeľa';
}

const getEasterMondayName = (locale = 'sk') => {
    if (locale === 'en') {
        return 'Easter Monday';
    }
    if (locale === 'de') {
        return 'Ostermontag';
    }
    return 'Veľkonočný pondelok';
}

module.exports = {
    getEasterMondayName,
    getGoodFridayName,
    getEasterSundayName,
}
