const getEasterSunday = require('./getEasterSunday');
const getEasterMonday = require('./getEasterMonday');
const getGoodFriday = require('./getGoodFriday');

const isEaster = (year, month, day) => {
    if (!day || !month || !year) {
        return false;
    }

    if (typeof day !== 'number' || typeof month !== 'number' || typeof year !== 'number') {
        return false;
    }

    if (year < 1993) {
        return false;
    }

    const easterSunday = getEasterSunday(year);
    const goodFriday = getGoodFriday(easterSunday);
    const easterMonday = getEasterMonday(easterSunday);
    return day === easterSunday.day && month === easterSunday.month
        || day === goodFriday.day && month === goodFriday.month
        || day === easterMonday.day && month === easterMonday.month;
}

module.exports = isEaster;
