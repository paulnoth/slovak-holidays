const getGoodFriday = easterSundayHoliday => {
    const month = easterSundayHoliday.day <= 2
        ? 3
        : easterSundayHoliday.month;
    let day;
    if (easterSundayHoliday.day === 1) {
        day = 30
    } else if (easterSundayHoliday.day === 2) {
        day = 31
    } else {
        day = easterSundayHoliday.day - 2;
    }
    return {
        day,
        month,
        year: easterSundayHoliday.year,
    }
};

module.exports = getGoodFriday;
