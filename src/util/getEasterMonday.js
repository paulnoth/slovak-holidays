const getEasterMonday = easterSundayHoliday => {
    const month = easterSundayHoliday.day === 31
        ? 4
        : easterSundayHoliday.month;
    const day = easterSundayHoliday.day === 31
        ? 1
        : easterSundayHoliday.day + 1;
    return {
        day,
        month,
        year: easterSundayHoliday.year,
    }
};

module.exports = getEasterMonday;
