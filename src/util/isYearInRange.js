const isYearInRange = (holiday, year) => year >= holiday.validFrom && year <= holiday.validTo;

module.exports = isYearInRange;
