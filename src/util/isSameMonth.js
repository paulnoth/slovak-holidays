const isSameMonth = (holiday, month) => holiday.month === month;

module.exports = isSameMonth;
