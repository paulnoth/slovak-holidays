// https://en.wikipedia.org/wiki/Computus#Gauss's_Easter_algorithm
const getEasterSunday = year => {
    if (!year) {
        return null;
    }

    if (typeof year !== 'number') {
        return null;
    }

    if (year < 1993) {
        return null;
    }

    const a = year % 19;
    const b = year % 4;
    const c = year % 7;
    const k = parseInt(year / 100);
    const p = parseInt((13 + 8 * k) / 25);
    const q = parseInt(k / 4);
    const M = (15 - p + k - q) % 30;
    const N = (4 + k - q) % 7;
    const d = (19 * a + M) % 30;
    const e = (2 * b + 4 * c + 6 * d + N) % 7;

    if (d === 28 && e === 6 && (11 * M + 11) % 30 < 19) {
        const april = 4;
        return {year, month: april, day: 18};
    }

    if (d === 29 && e === 6) {
        const april = 4;
        return {year, month: april, day: 19};
    }
    if (22 + d + e > 31) {
        const april = 4;
        return {year, month: april, day: d + e - 9};
    }
    const march = 3;
    const day = 22 + d + e;
    return {year, month: march, day};
};

module.exports = getEasterSunday;
