const parseDate = require('./util/parseDate');
const holidays = require('./util/holidays');
const isYearInRange = require('./util/isYearInRange');
const isSameMonth = require('./util/isSameMonth');
const isSameDay = require('./util/isSameDay');
const getEasterSunday = require('./util/getEasterSunday');
const getEasterMonday = require('./util/getEasterMonday');
const getGoodFriday = require('./util/getGoodFriday');
const {getGoodFridayName, getEasterSundayName, getEasterMondayName} = require('./util/easterNames');

const getHolidayName = (date, locale = 'sk') => {
    if (!date) {
        return null;
    }

    if (typeof date !== 'object') {
        return null;
    }

    const {year, month, day} = parseDate(date);

    const holiday = holidays.find(hol =>
        isYearInRange(hol, year)
        && isSameMonth(hol, month)
        && isSameDay(hol, day));

    if (holiday) {
        return holiday.name[locale];
    }

    const easterSunday = getEasterSunday(year);
    if (!easterSunday) {
        return null;
    }

    const goodFriday = getGoodFriday(easterSunday);
    const easterMonday = getEasterMonday(easterSunday);


    if (day === easterSunday.day && month === easterSunday.month) {
        return getEasterSundayName(locale);
    }
    if (day === goodFriday.day && month === goodFriday.month) {
        return getGoodFridayName(locale);
    }
    if (day === easterMonday.day && month === easterMonday.month) {
        return getEasterMondayName(locale);
    }
    return null;
}

module.exports = getHolidayName;
