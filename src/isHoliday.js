const holidays = require('./util/holidays');
const isYearInRange = require('./util/isYearInRange');
const isSameMonth = require('./util/isSameMonth');
const isSameDay = require('./util/isSameDay');
const isEaster = require('./util/isEaster');
const parseDate = require('./util/parseDate');

const isHoliday = date => {
    if (!date) {
        return false;
    }

    if (typeof date !== 'object') {
        return false;
    }

    const {year, month, day} = parseDate(date);

    return !!holidays
    .find(hol =>
        isYearInRange(hol, year)
        && isSameMonth(hol, month)
        && isSameDay(hol, day))
        || isEaster(year, month, day);
}

module.exports = isHoliday;
