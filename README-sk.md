# Slovenské sviatky
[English](./README.md)

NPM modul, ktorý dokáže vypočítať slovenské sviatky od roku 1993 do budúcnosti. Modul je čistý Node.js kód bez iných závislostí.

## Použitie
```
npm install slovak-holidays
```
```js
import {isHoliday, getAllHolidays, getHolidayName} from 'slovak-holidays';
```
## API

```js
isHoliday(date|object): boolean
```
Určí, či daný deň (`date|object`) je sviatok.

### parameter
 - `date` - inštancia Javascript Date objektu
 - `object` - "holiday" objekt `{year: integer, month: integer: day: integer}`

```js
isHoliday(new Date(1993, 0, 1)) // => true, Deň vzniku SR
isHoliday(new Date(1993, 0, 2)) // => false, bežný pracovný deň
isHoliday(new Date(2019, 3, 19)) // => true, Veľký piatok

isHoliday({year: 1993, month: 1,  day: 1}) // => true, Deň vzniku SR
isHoliday({year: 1993, month: 1,  day: 2}) // => false, bežný pracovný deň
isHoliday({year: 1993, month: 4,  day: 19}) // => true, Veľký piatok
```
--------------
```js
getAllHolidays(year): array
```
Vráti zoznam všetkých sviatkov pre daný rok (`year`) chronologicky.
```js
getAllHolidays(1992) // => [], rok je menší ako Deň vzniku SR (1993)
getAllHolidays(null) // => [], invalid year
getAllHolidays(2019)
// => [
//  {day: 1, month: 1, year: 2019, name: "Deň vzniku Slovenskej republiky"},
//  {day: 6, month: 1, year: 2019, name: "Zjavenie Pána (Traja králi)"},
//  {day: 19, month: 4, year: 2019, name: "Veľký piatok"},
//  {day: 21, month: 4, year: 2019, name: "Veľkonočná nedeľa"},
//  {day: 22, month: 4, year: 2019, name: "Veľkonočný pondelok"},
//  {day: 1, month: 5, year: 2019, name: "Sviatok práce" },
//  {day: 8, month: 5, year: 2019, name: "Deň víťazstva nad fašizmom"},
//  {day: 5, month: 7, year: 2019, name: "Sviatok svätého Cyrila a Metoda"},
//  {day: 29, month: 8, year: 2019, name: "Výročie SNP"},
//  {day: 1, month: 9, year: 2019, name: "Deň Ústavy Slovenskej republiky"},
//  {day: 15, month: 9, year: 2019, name: "Sedembolestná Panna Mária"},
//  {day: 1, month: 11, year: 2019, name: "Sviatok všetkých svätých"},
//  {day: 17, month: 11, year: 2019, name: "Deň boja za slobodu a demokraciu"},
//  {day: 24, month: 12, year: 2019, name: "Štedrý deň"},
//  {day: 25, month: 12, year: 2019, name: "Prvý sviatok vianočný"},
//  {day: 26, month: 12, year: 2019, name: "Druhý sviatok vianočný"}
// ]
```
```js
getHolidayName(date|object, locale = 'sk'): string|null
```
Vráti meno sviatku pre daný `date|object`, inak `null`.

### parameter
 - `date` - inštancia Javascript Date objektu
 - `object` - "holiday" objekt `{year: integer, month: integer: day: integer}`
 - `locale` - iso2 kód jazyka, do ktorého preloží meno sviatku (`'sk'`, `'en'`,`'de'` podporované)

```js
getHolidayName(new Date(1993, 0, 1)) // => "Deň vzniku Slovenskej republiky"
getHolidayName(new Date(1993, 0, 2)) // => null
getHolidayName(new Date(2019, 3, 19)) // => "Veľký piatok"

getHolidayName(new Date(1993, 0, 1), 'en') // => "Republic Day"
getHolidayName(new Date(1993, 0, 2), 'en') // => null
getHolidayName(new Date(2019, 3, 19), 'en') // => "Good Friday"

getHolidayName({year: 1993, month: 1,  day: 1}) // => "Deň vzniku Slovenskej republiky"
getHolidayName({year: 1993, month: 1,  day: 2}) // => null
getHolidayName({year: 1993, month: 4,  day: 19}) // => "Veľký piatok"
```