const isHoliday = require('./src/isHoliday');
const getAllHolidays = require('./src/getAllHolidays');
const getAllHolidayName = require('./src/getHolidayName');

module.exports = {
    isHoliday,
    getAllHolidays,
    getAllHolidayName,
}
