const getEasterSunday = require('../../src/util/getEasterSunday');

describe('getEasterSunday', () => {
    it('should return null when "year" parameter is not a number', () => {
        const result = getEasterSunday("2019");
        expect(result).toBeNull;
    });

    it('should return null when "year" parameter is null', () => {
        const result = getEasterSunday(null);
        expect(result).toBeNull;
    });

    it('should return null when "year" parameter is empty', () => {
        const result = getEasterSunday();
        expect(result).toBeNull;
    });

    it('should return null for the Easter Sunday before year 1993', () => {
        const easterSunday = getEasterSunday(1992);
        expect(easterSunday).toBeNull;
    });

    it('should return 11th April as the Easter Sunday for the year 1993', () => {
        const easterSunday = getEasterSunday(1993);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(11);
    });

    it('should return 3rd April as the Easter Sunday for the year 1994', () => {
        const easterSunday = getEasterSunday(1994);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(3);
    });

    it('should return 7th April as the Easter Sunday for the year 1996', () => {
        const easterSunday = getEasterSunday(1996);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(7);
    });

    it('should return 4th April as the Easter Sunday for the year 1999', () => {
        const easterSunday = getEasterSunday(1999);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(4);
    });


    it('should return 15th April as the Easter Sunday for the year 2001', () => {
        const easterSunday = getEasterSunday(2001);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(15);
    });


    it('should return 27th April as the Easter Sunday for the year 2005', () => {
        const easterSunday = getEasterSunday(2005);
        expect(easterSunday.month).toBe(3);
        expect(easterSunday.day).toBe(27);
    });

    it('should return 4th April as the Easter Sunday for the year 2010', () => {
        const easterSunday = getEasterSunday(2010);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(4);
    });

    it('should return 20th April as the Easter Sunday for the year 2014', () => {
        const easterSunday = getEasterSunday(2014);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(20);
    });

    it('should return 16th April as the Easter Sunday for the year 2017', () => {
        const easterSunday = getEasterSunday(2017);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(16);
    });

    it('should return 1st April as the Easter Sunday for the year 2018', () => {
        const easterSunday = getEasterSunday(2018);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(1);
    });

    it('should return 21st April as the Easter Sunday for the year 2019', () => {
        const easterSunday = getEasterSunday(2019);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(21);
    });

    it('should return 12th April as the Easter Sunday for the year 2020', () => {
        const easterSunday = getEasterSunday(2020);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(12);
    });

    it('should return 4th April as the Easter Sunday for the year 2021', () => {
        const easterSunday = getEasterSunday(2021);
        expect(easterSunday.month).toBe(4);
        expect(easterSunday.day).toBe(4);
    });
});