const isEaster = require('../../src/util/isEaster');

describe('isEaster', () => {
    it('String year is not valid for isEaster', () => {
		expect(isEaster("1993", 1, 1)).toBe(false);
    });

    it('String month is not valid for isEaster', () => {
		expect(isEaster(1993, "1", 1)).toBe(false);
    });

    it('String day is not valid for isEaster', () => {
		expect(isEaster(1993, 1, "1")).toBe(false);
    });

    it('empty parameter is not valid for isEaster', () => {
		expect(isEaster()).toBe(false);
    });

	it('1.1.1992 is not the Easter', () => {
		expect(isEaster(1992, 1, 1)).toBe(false);
	});

	it('6.1.1992 is not the Easter', () => {
		expect(isEaster(1992, 1, 6)).toBe(false);
	});

	it('9.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 9)).toBe(false);
	});

	it('11.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 9)).toBe(false);
	});

	it('12.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 12)).toBe(false);
	});

	it('1.5.1992 is not the Easter', () => {
		expect(isEaster(1992, 5, 1)).toBe(false);
	});

	it('8.5.1992 is not the Easter', () => {
		expect(isEaster(1992, 5, 8)).toBe(false);
	});

	it('5.7.1992 is not the Easter', () => {
		expect(isEaster(1992, 7, 5)).toBe(false);
	});

	it('29.8.1992 is not the Easter', () => {
		expect(isEaster(1992, 8, 29)).toBe(false);
	});

	it('1.9.1992 is not the Easter', () => {
		expect(isEaster(1992, 9, 1)).toBe(false);
	});

	it('15.9.1992 is not the Easter', () => {
		expect(isEaster(1992, 9, 15)).toBe(false);
	});

	it('1.11.1992 is not the Easter', () => {
		expect(isEaster(1992, 11, 1)).toBe(false);
	});

	it('17.11.1992 is not the Easter', () => {
		expect(isEaster(1992, 11, 17)).toBe(false);
	});

	it('24.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 24)).toBe(false);
	});

	it('25.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 25)).toBe(false);
	});

	it('26.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 26)).toBe(false);
	});
	
	it('1.1.1993 is not the Easter', () => {
		expect(isEaster(1993, 1, 1)).toBe(false);
	});

	it('6.1.1993 is not the Easter', () => {
		expect(isEaster(1993, 1, 6)).toBe(false);
	});

	it('9.4.1993 is the Easter', () => {
		expect(isEaster(1993, 4, 9)).toBe(true);
	});

	it('11.4.1993 is the Easter', () => {
		expect(isEaster(1993, 4, 11)).toBe(true);
	});

	it('12.4.1993 is the Easter', () => {
		expect(isEaster(1993, 4, 12)).toBe(true);
	});

	it('1.5.1993 is not the Easter', () => {
		expect(isEaster(1993, 5, 1)).toBe(false);
	});

	it('8.5.1993 is not the Easter', () => {
		expect(isEaster(1993, 5, 8)).toBe(false);
	});

	it('5.7.1993 is not the Easter', () => {
		expect(isEaster(1993, 7, 5)).toBe(false);
	});

	it('29.8.1993 is not the Easter', () => {
		expect(isEaster(1993, 8, 29)).toBe(false);
	});

	it('1.9.1993 is not the Easter', () => {
		expect(isEaster(1993, 9, 1)).toBe(false);
	});

	it('15.9.1993 is not the Easter', () => {
		expect(isEaster(1993, 9, 15)).toBe(false);
	});

	it('1.11.1993 is not the Easter', () => {
		expect(isEaster(1993, 11, 1)).toBe(false);
	});

	it('17.11.1993 is not the Easter', () => {
		expect(isEaster(1993, 11, 17)).toBe(false);
	});

	it('24.12.1993 is not the Easter', () => {
		expect(isEaster(1993, 12, 24)).toBe(false);
	});

	it('25.12.1993 is not the Easter', () => {
		expect(isEaster(1993, 12, 25)).toBe(false);
	});

	it('26.12.1993 is not the Easter', () => {
		expect(isEaster(1993, 12, 26)).toBe(false);
	});

	it('1.1.1994 is not the Easter', () => {
		expect(isEaster(1994, 1, 1)).toBe(false);
	});

	it('6.1.1994 is not the Easter', () => {
		expect(isEaster(1994, 1, 6)).toBe(false);
	});

	it('1.4.1994 is the Easter', () => {
		expect(isEaster(1994, 4, 1)).toBe(true);
	});

	it('3.4.1994 is the Easter', () => {
		expect(isEaster(1994, 4, 3)).toBe(true);
	});

	it('4.4.1994 is the Easter', () => {
		expect(isEaster(1994, 4, 4)).toBe(true);
	});

	it('1.5.1994 is not the Easter', () => {
		expect(isEaster(1994, 5, 1)).toBe(false);
	});

	it('8.5.1994 is not the Easter', () => {
		expect(isEaster(1994, 5, 8)).toBe(false);
	});

	it('5.7.1994 is not the Easter', () => {
		expect(isEaster(1994, 7, 5)).toBe(false);
	});

	it('29.8.1994 is not the Easter', () => {
		expect(isEaster(1994, 8, 29)).toBe(false);
	});

	it('1.9.1994 is not the Easter', () => {
		expect(isEaster(1994, 9, 1)).toBe(false);
	});

	it('15.9.1994 is not the Easter', () => {
		expect(isEaster(1994, 9, 15)).toBe(false);
	});

	it('1.11.1994 is not the Easter', () => {
		expect(isEaster(1994, 11, 1)).toBe(false);
	});

	it('17.11.1994 is not the Easter', () => {
		expect(isEaster(1994, 11, 17)).toBe(false);
	});

	it('24.12.1994 is not the Easter', () => {
		expect(isEaster(1994, 12, 24)).toBe(false);
	});

	it('25.12.1994 is not the Easter', () => {
		expect(isEaster(1994, 12, 25)).toBe(false);
	});

	it('26.12.1994 is not the Easter', () => {
		expect(isEaster(1994, 12, 26)).toBe(false);
	});

	it('1.1.1995 is not the Easter', () => {
		expect(isEaster(1995, 1, 1)).toBe(false);
	});

	it('6.1.1995 is not the Easter', () => {
		expect(isEaster(1995, 1, 6)).toBe(false);
	});

	it('14.4.1995 is the Easter', () => {
		expect(isEaster(1995, 4, 14)).toBe(true);
	});

	it('16.4.1995 is the Easter', () => {
		expect(isEaster(1995, 4, 16)).toBe(true);
	});

	it('17.4.1995 is the Easter', () => {
		expect(isEaster(1995, 4, 17)).toBe(true);
	});

	it('1.5.1995 is not the Easter', () => {
		expect(isEaster(1995, 5, 1)).toBe(false);
	});

	it('8.5.1995 is not the Easter', () => {
		expect(isEaster(1995, 5, 8)).toBe(false);
	});

	it('5.7.1995 is not the Easter', () => {
		expect(isEaster(1995, 7, 5)).toBe(false);
	});

	it('29.8.1995 is not the Easter', () => {
		expect(isEaster(1995, 8, 29)).toBe(false);
	});

	it('1.9.1995 is not the Easter', () => {
		expect(isEaster(1995, 9, 1)).toBe(false);
	});

	it('15.9.1995 is not the Easter', () => {
		expect(isEaster(1995, 9, 15)).toBe(false);
	});

	it('1.11.1995 is not the Easter', () => {
		expect(isEaster(1995, 11, 1)).toBe(false);
	});

	it('17.11.1995 is not the Easter', () => {
		expect(isEaster(1995, 11, 17)).toBe(false);
	});

	it('24.12.1995 is not the Easter', () => {
		expect(isEaster(1995, 12, 24)).toBe(false);
	});

	it('25.12.1995 is not the Easter', () => {
		expect(isEaster(1995, 12, 25)).toBe(false);
	});

	it('26.12.1995 is not the Easter', () => {
		expect(isEaster(1995, 12, 26)).toBe(false);
	});

	it('1.1.1996 is not the Easter', () => {
		expect(isEaster(1996, 1, 1)).toBe(false);
	});

	it('6.1.1996 is not the Easter', () => {
		expect(isEaster(1996, 1, 6)).toBe(false);
	});

	it('5.4.1996 is the Easter', () => {
		expect(isEaster(1996, 4, 5)).toBe(true);
	});

	it('7.4.1996 is the Easter', () => {
		expect(isEaster(1996, 4, 7)).toBe(true);
	});

	it('8.4.1996 is the Easter', () => {
		expect(isEaster(1996, 4, 8)).toBe(true);
	});

	it('1.5.1996 is not the Easter', () => {
		expect(isEaster(1996, 5, 1)).toBe(false);
	});

	it('8.5.1996 is not the Easter', () => {
		expect(isEaster(1996, 5, 8)).toBe(false);
	});

	it('5.7.1996 is not the Easter', () => {
		expect(isEaster(1996, 7, 5)).toBe(false);
	});

	it('29.8.1996 is not the Easter', () => {
		expect(isEaster(1996, 8, 29)).toBe(false);
	});

	it('1.9.1996 is not the Easter', () => {
		expect(isEaster(1996, 9, 1)).toBe(false);
	});

	it('15.9.1996 is not the Easter', () => {
		expect(isEaster(1996, 9, 15)).toBe(false);
	});

	it('1.11.1996 is not the Easter', () => {
		expect(isEaster(1996, 11, 1)).toBe(false);
	});

	it('17.11.1996 is not the Easter', () => {
		expect(isEaster(1996, 11, 17)).toBe(false);
	});

	it('24.12.1996 is not the Easter', () => {
		expect(isEaster(1996, 12, 24)).toBe(false);
	});

	it('25.12.1996 is not the Easter', () => {
		expect(isEaster(1996, 12, 25)).toBe(false);
	});

	it('26.12.1996 is not the Easter', () => {
		expect(isEaster(1996, 12, 26)).toBe(false);
	});

	it('1.1.1997 is not the Easter', () => {
		expect(isEaster(1997, 1, 1)).toBe(false);
	});

	it('6.1.1997 is not the Easter', () => {
		expect(isEaster(1997, 1, 6)).toBe(false);
	});

	it('28.3.1997 is the Easter', () => {
		expect(isEaster(1997, 3, 28)).toBe(true);
	});

	it('30.3.1997 is the Easter', () => {
		expect(isEaster(1997, 3, 30)).toBe(true);
	});

	it('31.3.1997 is the Easter', () => {
		expect(isEaster(1997, 3, 31)).toBe(true);
	});

	it('1.5.1997 is not the Easter', () => {
		expect(isEaster(1997, 5, 1)).toBe(false);
	});

	it('8.5.1997 is not the Easter', () => {
		expect(isEaster(1997, 5, 8)).toBe(false);
	});

	it('5.7.1997 is not the Easter', () => {
		expect(isEaster(1997, 7, 5)).toBe(false);
	});

	it('29.8.1997 is not the Easter', () => {
		expect(isEaster(1997, 8, 29)).toBe(false);
	});

	it('1.9.1997 is not the Easter', () => {
		expect(isEaster(1997, 9, 1)).toBe(false);
	});

	it('15.9.1997 is not the Easter', () => {
		expect(isEaster(1997, 9, 15)).toBe(false);
	});

	it('1.11.1997 is not the Easter', () => {
		expect(isEaster(1997, 11, 1)).toBe(false);
	});

	it('17.11.1997 is not the Easter', () => {
		expect(isEaster(1997, 11, 17)).toBe(false);
	});

	it('24.12.1997 is not the Easter', () => {
		expect(isEaster(1997, 12, 24)).toBe(false);
	});

	it('25.12.1997 is not the Easter', () => {
		expect(isEaster(1997, 12, 25)).toBe(false);
	});

	it('26.12.1997 is not the Easter', () => {
		expect(isEaster(1997, 12, 26)).toBe(false);
	});

	it('1.1.1998 is not the Easter', () => {
		expect(isEaster(1998, 1, 1)).toBe(false);
	});

	it('6.1.1998 is not the Easter', () => {
		expect(isEaster(1998, 1, 6)).toBe(false);
	});

	it('10.4.1998 is the Easter', () => {
		expect(isEaster(1998, 4, 10)).toBe(true);
	});

	it('12.4.1998 is the Easter', () => {
		expect(isEaster(1998, 4, 12)).toBe(true);
	});

	it('13.4.1998 is the Easter', () => {
		expect(isEaster(1998, 4, 13)).toBe(true);
	});

	it('1.5.1998 is not the Easter', () => {
		expect(isEaster(1998, 5, 1)).toBe(false);
	});

	it('8.5.1998 is not the Easter', () => {
		expect(isEaster(1998, 5, 8)).toBe(false);
	});

	it('5.7.1998 is not the Easter', () => {
		expect(isEaster(1998, 7, 5)).toBe(false);
	});

	it('29.8.1998 is not the Easter', () => {
		expect(isEaster(1998, 8, 29)).toBe(false);
	});

	it('1.9.1998 is not the Easter', () => {
		expect(isEaster(1998, 9, 1)).toBe(false);
	});

	it('15.9.1998 is not the Easter', () => {
		expect(isEaster(1998, 9, 15)).toBe(false);
	});

	it('1.11.1998 is not the Easter', () => {
		expect(isEaster(1998, 11, 1)).toBe(false);
	});

	it('17.11.1998 is not the Easter', () => {
		expect(isEaster(1998, 11, 17)).toBe(false);
	});

	it('24.12.1998 is not the Easter', () => {
		expect(isEaster(1998, 12, 24)).toBe(false);
	});

	it('25.12.1998 is not the Easter', () => {
		expect(isEaster(1998, 12, 25)).toBe(false);
	});

	it('26.12.1998 is not the Easter', () => {
		expect(isEaster(1998, 12, 26)).toBe(false);
	});

	it('1.1.1999 is not the Easter', () => {
		expect(isEaster(1999, 1, 1)).toBe(false);
	});

	it('6.1.1999 is not the Easter', () => {
		expect(isEaster(1999, 1, 6)).toBe(false);
	});

	it('2.4.1999 is the Easter', () => {
		expect(isEaster(1999, 4, 2)).toBe(true);
	});

	it('4.4.1999 is the Easter', () => {
		expect(isEaster(1999, 4, 4)).toBe(true);
	});

	it('5.4.1999 is the Easter', () => {
		expect(isEaster(1999, 4, 5)).toBe(true);
	});

	it('1.5.1999 is not the Easter', () => {
		expect(isEaster(1999, 5, 1)).toBe(false);
	});

	it('8.5.1999 is not the Easter', () => {
		expect(isEaster(1999, 5, 8)).toBe(false);
	});

	it('5.7.1999 is not the Easter', () => {
		expect(isEaster(1999, 7, 5)).toBe(false);
	});

	it('29.8.1999 is not the Easter', () => {
		expect(isEaster(1999, 8, 29)).toBe(false);
	});

	it('1.9.1999 is not the Easter', () => {
		expect(isEaster(1999, 9, 1)).toBe(false);
	});

	it('15.9.1999 is not the Easter', () => {
		expect(isEaster(1999, 9, 15)).toBe(false);
	});

	it('1.11.1999 is not the Easter', () => {
		expect(isEaster(1999, 11, 1)).toBe(false);
	});

	it('17.11.1999 is not the Easter', () => {
		expect(isEaster(1999, 11, 17)).toBe(false);
	});

	it('24.12.1999 is not the Easter', () => {
		expect(isEaster(1999, 12, 24)).toBe(false);
	});

	it('25.12.1999 is not the Easter', () => {
		expect(isEaster(1999, 12, 25)).toBe(false);
	});

	it('26.12.1999 is not the Easter', () => {
		expect(isEaster(1999, 12, 26)).toBe(false);
	});

	it('1.1.2000 is not the Easter', () => {
		expect(isEaster(2000, 1, 1)).toBe(false);
	});

	it('6.1.2000 is not the Easter', () => {
		expect(isEaster(2000, 1, 6)).toBe(false);
	});

	it('21.4.2000 is the Easter', () => {
		expect(isEaster(2000, 4, 21)).toBe(true);
	});

	it('23.4.2000 is the Easter', () => {
		expect(isEaster(2000, 4, 23)).toBe(true);
	});

	it('24.4.2000 is the Easter', () => {
		expect(isEaster(2000, 4, 24)).toBe(true);
	});

	it('1.5.2000 is not the Easter', () => {
		expect(isEaster(2000, 5, 1)).toBe(false);
	});

	it('8.5.2000 is not the Easter', () => {
		expect(isEaster(2000, 5, 8)).toBe(false);
	});

	it('5.7.2000 is not the Easter', () => {
		expect(isEaster(2000, 7, 5)).toBe(false);
	});

	it('29.8.2000 is not the Easter', () => {
		expect(isEaster(2000, 8, 29)).toBe(false);
	});

	it('1.9.2000 is not the Easter', () => {
		expect(isEaster(2000, 9, 1)).toBe(false);
	});

	it('15.9.2000 is not the Easter', () => {
		expect(isEaster(2000, 9, 15)).toBe(false);
	});

	it('1.11.2000 is not the Easter', () => {
		expect(isEaster(2000, 11, 1)).toBe(false);
	});

	it('17.11.2000 is not the Easter', () => {
		expect(isEaster(2000, 11, 17)).toBe(false);
	});

	it('24.12.2000 is not the Easter', () => {
		expect(isEaster(2000, 12, 24)).toBe(false);
	});

	it('25.12.2000 is not the Easter', () => {
		expect(isEaster(2000, 12, 25)).toBe(false);
	});

	it('26.12.2000 is not the Easter', () => {
		expect(isEaster(2000, 12, 26)).toBe(false);
	});

	it('1.1.2001 is not the Easter', () => {
		expect(isEaster(2001, 1, 1)).toBe(false);
	});

	it('6.1.2001 is not the Easter', () => {
		expect(isEaster(2001, 1, 6)).toBe(false);
	});

	it('13.4.2001 is the Easter', () => {
		expect(isEaster(2001, 4, 13)).toBe(true);
	});

	it('15.4.2001 is the Easter', () => {
		expect(isEaster(2001, 4, 15)).toBe(true);
	});

	it('16.4.2001 is the Easter', () => {
		expect(isEaster(2001, 4, 16)).toBe(true);
	});

	it('1.5.2001 is not the Easter', () => {
		expect(isEaster(2001, 5, 1)).toBe(false);
	});

	it('8.5.2001 is not the Easter', () => {
		expect(isEaster(2001, 5, 8)).toBe(false);
	});

	it('5.7.2001 is not the Easter', () => {
		expect(isEaster(2001, 7, 5)).toBe(false);
	});

	it('29.8.2001 is not the Easter', () => {
		expect(isEaster(2001, 8, 29)).toBe(false);
	});

	it('1.9.2001 is not the Easter', () => {
		expect(isEaster(2001, 9, 1)).toBe(false);
	});

	it('15.9.2001 is not the Easter', () => {
		expect(isEaster(2001, 9, 15)).toBe(false);
	});

	it('1.11.2001 is not the Easter', () => {
		expect(isEaster(2001, 11, 1)).toBe(false);
	});

	it('17.11.2001 is not the Easter', () => {
		expect(isEaster(2001, 11, 17)).toBe(false);
	});

	it('24.12.2001 is not the Easter', () => {
		expect(isEaster(2001, 12, 24)).toBe(false);
	});

	it('25.12.2001 is not the Easter', () => {
		expect(isEaster(2001, 12, 25)).toBe(false);
	});

	it('26.12.2001 is not the Easter', () => {
		expect(isEaster(2001, 12, 26)).toBe(false);
	});

	it('1.1.2002 is not the Easter', () => {
		expect(isEaster(2002, 1, 1)).toBe(false);
	});

	it('6.1.2002 is not the Easter', () => {
		expect(isEaster(2002, 1, 6)).toBe(false);
	});

	it('29.3.2002 is the Easter', () => {
		expect(isEaster(2002, 3, 29)).toBe(true);
	});

	it('31.3.2002 is the Easter', () => {
		expect(isEaster(2002, 3, 31)).toBe(true);
	});

	it('1.4.2002 is the Easter', () => {
		expect(isEaster(2002, 4, 1)).toBe(true);
	});

	it('1.5.2002 is not the Easter', () => {
		expect(isEaster(2002, 5, 1)).toBe(false);
	});

	it('8.5.2002 is not the Easter', () => {
		expect(isEaster(2002, 5, 8)).toBe(false);
	});

	it('5.7.2002 is not the Easter', () => {
		expect(isEaster(2002, 7, 5)).toBe(false);
	});

	it('29.8.2002 is not the Easter', () => {
		expect(isEaster(2002, 8, 29)).toBe(false);
	});

	it('1.9.2002 is not the Easter', () => {
		expect(isEaster(2002, 9, 1)).toBe(false);
	});

	it('15.9.2002 is not the Easter', () => {
		expect(isEaster(2002, 9, 15)).toBe(false);
	});

	it('1.11.2002 is not the Easter', () => {
		expect(isEaster(2002, 11, 1)).toBe(false);
	});

	it('17.11.2002 is not the Easter', () => {
		expect(isEaster(2002, 11, 17)).toBe(false);
	});

	it('24.12.2002 is not the Easter', () => {
		expect(isEaster(2002, 12, 24)).toBe(false);
	});

	it('25.12.2002 is not the Easter', () => {
		expect(isEaster(2002, 12, 25)).toBe(false);
	});

	it('26.12.2002 is not the Easter', () => {
		expect(isEaster(2002, 12, 26)).toBe(false);
	});

	it('1.1.2003 is not the Easter', () => {
		expect(isEaster(2003, 1, 1)).toBe(false);
	});

	it('6.1.2003 is not the Easter', () => {
		expect(isEaster(2003, 1, 6)).toBe(false);
	});

	it('18.4.2003 is the Easter', () => {
		expect(isEaster(2003, 4, 18)).toBe(true);
	});

	it('20.4.2003 is the Easter', () => {
		expect(isEaster(2003, 4, 20)).toBe(true);
	});

	it('21.4.2003 is the Easter', () => {
		expect(isEaster(2003, 4, 21)).toBe(true);
	});

	it('1.5.2003 is not the Easter', () => {
		expect(isEaster(2003, 5, 1)).toBe(false);
	});

	it('8.5.2003 is not the Easter', () => {
		expect(isEaster(2003, 5, 8)).toBe(false);
	});

	it('5.7.2003 is not the Easter', () => {
		expect(isEaster(2003, 7, 5)).toBe(false);
	});

	it('29.8.2003 is not the Easter', () => {
		expect(isEaster(2003, 8, 29)).toBe(false);
	});

	it('1.9.2003 is not the Easter', () => {
		expect(isEaster(2003, 9, 1)).toBe(false);
	});

	it('15.9.2003 is not the Easter', () => {
		expect(isEaster(2003, 9, 15)).toBe(false);
	});

	it('1.11.2003 is not the Easter', () => {
		expect(isEaster(2003, 11, 1)).toBe(false);
	});

	it('17.11.2003 is not the Easter', () => {
		expect(isEaster(2003, 11, 17)).toBe(false);
	});

	it('24.12.2003 is not the Easter', () => {
		expect(isEaster(2003, 12, 24)).toBe(false);
	});

	it('25.12.2003 is not the Easter', () => {
		expect(isEaster(2003, 12, 25)).toBe(false);
	});

	it('26.12.2003 is not the Easter', () => {
		expect(isEaster(2003, 12, 26)).toBe(false);
	});

	it('1.1.2004 is not the Easter', () => {
		expect(isEaster(2004, 1, 1)).toBe(false);
	});

	it('6.1.2004 is not the Easter', () => {
		expect(isEaster(2004, 1, 6)).toBe(false);
	});

	it('9.4.2004 is the Easter', () => {
		expect(isEaster(2004, 4, 9)).toBe(true);
	});

	it('11.4.2004 is the Easter', () => {
		expect(isEaster(2004, 4, 11)).toBe(true);
	});

	it('12.4.2004 is the Easter', () => {
		expect(isEaster(2004, 4, 12)).toBe(true);
	});

	it('1.5.2004 is not the Easter', () => {
		expect(isEaster(2004, 5, 1)).toBe(false);
	});

	it('8.5.2004 is not the Easter', () => {
		expect(isEaster(2004, 5, 8)).toBe(false);
	});

	it('5.7.2004 is not the Easter', () => {
		expect(isEaster(2004, 7, 5)).toBe(false);
	});

	it('29.8.2004 is not the Easter', () => {
		expect(isEaster(2004, 8, 29)).toBe(false);
	});

	it('1.9.2004 is not the Easter', () => {
		expect(isEaster(2004, 9, 1)).toBe(false);
	});

	it('15.9.2004 is not the Easter', () => {
		expect(isEaster(2004, 9, 15)).toBe(false);
	});

	it('1.11.2004 is not the Easter', () => {
		expect(isEaster(2004, 11, 1)).toBe(false);
	});

	it('17.11.2004 is not the Easter', () => {
		expect(isEaster(2004, 11, 17)).toBe(false);
	});

	it('24.12.2004 is not the Easter', () => {
		expect(isEaster(2004, 12, 24)).toBe(false);
	});

	it('25.12.2004 is not the Easter', () => {
		expect(isEaster(2004, 12, 25)).toBe(false);
	});

	it('26.12.2004 is not the Easter', () => {
		expect(isEaster(2004, 12, 26)).toBe(false);
	});

	it('1.1.2005 is not the Easter', () => {
		expect(isEaster(2005, 1, 1)).toBe(false);
	});

	it('6.1.2005 is not the Easter', () => {
		expect(isEaster(2005, 1, 6)).toBe(false);
	});

	it('25.3.2005 is the Easter', () => {
		expect(isEaster(2005, 3, 25)).toBe(true);
	});

	it('27.3.2005 is the Easter', () => {
		expect(isEaster(2005, 3, 27)).toBe(true);
	});

	it('28.3.2005 is the Easter', () => {
		expect(isEaster(2005, 3, 28)).toBe(true);
	});

	it('1.5.2005 is not the Easter', () => {
		expect(isEaster(2005, 5, 1)).toBe(false);
	});

	it('8.5.2005 is not the Easter', () => {
		expect(isEaster(2005, 5, 8)).toBe(false);
	});

	it('5.7.2005 is not the Easter', () => {
		expect(isEaster(2005, 7, 5)).toBe(false);
	});

	it('29.8.2005 is not the Easter', () => {
		expect(isEaster(2005, 8, 29)).toBe(false);
	});

	it('1.9.2005 is not the Easter', () => {
		expect(isEaster(2005, 9, 1)).toBe(false);
	});

	it('15.9.2005 is not the Easter', () => {
		expect(isEaster(2005, 9, 15)).toBe(false);
	});

	it('1.11.2005 is not the Easter', () => {
		expect(isEaster(2005, 11, 1)).toBe(false);
	});

	it('17.11.2005 is not the Easter', () => {
		expect(isEaster(2005, 11, 17)).toBe(false);
	});

	it('24.12.2005 is not the Easter', () => {
		expect(isEaster(2005, 12, 24)).toBe(false);
	});

	it('25.12.2005 is not the Easter', () => {
		expect(isEaster(2005, 12, 25)).toBe(false);
	});

	it('26.12.2005 is not the Easter', () => {
		expect(isEaster(2005, 12, 26)).toBe(false);
	});

	it('1.1.2006 is not the Easter', () => {
		expect(isEaster(2006, 1, 1)).toBe(false);
	});

	it('6.1.2006 is not the Easter', () => {
		expect(isEaster(2006, 1, 6)).toBe(false);
	});

	it('14.4.2006 is the Easter', () => {
		expect(isEaster(2006, 4, 14)).toBe(true);
	});

	it('16.4.2006 is the Easter', () => {
		expect(isEaster(2006, 4, 16)).toBe(true);
	});

	it('17.4.2006 is the Easter', () => {
		expect(isEaster(2006, 4, 17)).toBe(true);
	});

	it('1.5.2006 is not the Easter', () => {
		expect(isEaster(2006, 5, 1)).toBe(false);
	});

	it('8.5.2006 is not the Easter', () => {
		expect(isEaster(2006, 5, 8)).toBe(false);
	});

	it('5.7.2006 is not the Easter', () => {
		expect(isEaster(2006, 7, 5)).toBe(false);
	});

	it('29.8.2006 is not the Easter', () => {
		expect(isEaster(2006, 8, 29)).toBe(false);
	});

	it('1.9.2006 is not the Easter', () => {
		expect(isEaster(2006, 9, 1)).toBe(false);
	});

	it('15.9.2006 is not the Easter', () => {
		expect(isEaster(2006, 9, 15)).toBe(false);
	});

	it('1.11.2006 is not the Easter', () => {
		expect(isEaster(2006, 11, 1)).toBe(false);
	});

	it('17.11.2006 is not the Easter', () => {
		expect(isEaster(2006, 11, 17)).toBe(false);
	});

	it('24.12.2006 is not the Easter', () => {
		expect(isEaster(2006, 12, 24)).toBe(false);
	});

	it('25.12.2006 is not the Easter', () => {
		expect(isEaster(2006, 12, 25)).toBe(false);
	});

	it('26.12.2006 is not the Easter', () => {
		expect(isEaster(2006, 12, 26)).toBe(false);
	});

	it('1.1.2007 is not the Easter', () => {
		expect(isEaster(2007, 1, 1)).toBe(false);
	});

	it('6.1.2007 is not the Easter', () => {
		expect(isEaster(2007, 1, 6)).toBe(false);
	});

	it('6.4.2007 is the Easter', () => {
		expect(isEaster(2007, 4, 6)).toBe(true);
	});

	it('8.4.2007 is the Easter', () => {
		expect(isEaster(2007, 4, 8)).toBe(true);
	});

	it('9.4.2007 is the Easter', () => {
		expect(isEaster(2007, 4, 9)).toBe(true);
	});

	it('1.5.2007 is not the Easter', () => {
		expect(isEaster(2007, 5, 1)).toBe(false);
	});

	it('8.5.2007 is not the Easter', () => {
		expect(isEaster(2007, 5, 8)).toBe(false);
	});

	it('5.7.2007 is not the Easter', () => {
		expect(isEaster(2007, 7, 5)).toBe(false);
	});

	it('29.8.2007 is not the Easter', () => {
		expect(isEaster(2007, 8, 29)).toBe(false);
	});

	it('1.9.2007 is not the Easter', () => {
		expect(isEaster(2007, 9, 1)).toBe(false);
	});

	it('15.9.2007 is not the Easter', () => {
		expect(isEaster(2007, 9, 15)).toBe(false);
	});

	it('1.11.2007 is not the Easter', () => {
		expect(isEaster(2007, 11, 1)).toBe(false);
	});

	it('17.11.2007 is not the Easter', () => {
		expect(isEaster(2007, 11, 17)).toBe(false);
	});

	it('24.12.2007 is not the Easter', () => {
		expect(isEaster(2007, 12, 24)).toBe(false);
	});

	it('25.12.2007 is not the Easter', () => {
		expect(isEaster(2007, 12, 25)).toBe(false);
	});

	it('26.12.2007 is not the Easter', () => {
		expect(isEaster(2007, 12, 26)).toBe(false);
	});

	it('1.1.2008 is not the Easter', () => {
		expect(isEaster(2008, 1, 1)).toBe(false);
	});

	it('6.1.2008 is not the Easter', () => {
		expect(isEaster(2008, 1, 6)).toBe(false);
	});

	it('21.3.2008 is the Easter', () => {
		expect(isEaster(2008, 3, 21)).toBe(true);
	});

	it('23.3.2008 is the Easter', () => {
		expect(isEaster(2008, 3, 23)).toBe(true);
	});

	it('24.3.2008 is the Easter', () => {
		expect(isEaster(2008, 3, 24)).toBe(true);
	});

	it('1.5.2008 is not the Easter', () => {
		expect(isEaster(2008, 5, 1)).toBe(false);
	});

	it('8.5.2008 is not the Easter', () => {
		expect(isEaster(2008, 5, 8)).toBe(false);
	});

	it('5.7.2008 is not the Easter', () => {
		expect(isEaster(2008, 7, 5)).toBe(false);
	});

	it('29.8.2008 is not the Easter', () => {
		expect(isEaster(2008, 8, 29)).toBe(false);
	});

	it('1.9.2008 is not the Easter', () => {
		expect(isEaster(2008, 9, 1)).toBe(false);
	});

	it('15.9.2008 is not the Easter', () => {
		expect(isEaster(2008, 9, 15)).toBe(false);
	});

	it('1.11.2008 is not the Easter', () => {
		expect(isEaster(2008, 11, 1)).toBe(false);
	});

	it('17.11.2008 is not the Easter', () => {
		expect(isEaster(2008, 11, 17)).toBe(false);
	});

	it('24.12.2008 is not the Easter', () => {
		expect(isEaster(2008, 12, 24)).toBe(false);
	});

	it('25.12.2008 is not the Easter', () => {
		expect(isEaster(2008, 12, 25)).toBe(false);
	});

	it('26.12.2008 is not the Easter', () => {
		expect(isEaster(2008, 12, 26)).toBe(false);
	});

	it('1.1.2009 is not the Easter', () => {
		expect(isEaster(2009, 1, 1)).toBe(false);
	});

	it('6.1.2009 is not the Easter', () => {
		expect(isEaster(2009, 1, 6)).toBe(false);
	});

	it('10.4.2009 is the Easter', () => {
		expect(isEaster(2009, 4, 10)).toBe(true);
	});

	it('12.4.2009 is the Easter', () => {
		expect(isEaster(2009, 4, 12)).toBe(true);
	});

	it('13.4.2009 is the Easter', () => {
		expect(isEaster(2009, 4, 13)).toBe(true);
	});

	it('1.5.2009 is not the Easter', () => {
		expect(isEaster(2009, 5, 1)).toBe(false);
	});

	it('8.5.2009 is not the Easter', () => {
		expect(isEaster(2009, 5, 8)).toBe(false);
	});

	it('5.7.2009 is not the Easter', () => {
		expect(isEaster(2009, 7, 5)).toBe(false);
	});

	it('29.8.2009 is not the Easter', () => {
		expect(isEaster(2009, 8, 29)).toBe(false);
	});

	it('1.9.2009 is not the Easter', () => {
		expect(isEaster(2009, 9, 1)).toBe(false);
	});

	it('15.9.2009 is not the Easter', () => {
		expect(isEaster(2009, 9, 15)).toBe(false);
	});

	it('1.11.2009 is not the Easter', () => {
		expect(isEaster(2009, 11, 1)).toBe(false);
	});

	it('17.11.2009 is not the Easter', () => {
		expect(isEaster(2009, 11, 17)).toBe(false);
	});

	it('24.12.2009 is not the Easter', () => {
		expect(isEaster(2009, 12, 24)).toBe(false);
	});

	it('25.12.2009 is not the Easter', () => {
		expect(isEaster(2009, 12, 25)).toBe(false);
	});

	it('26.12.2009 is not the Easter', () => {
		expect(isEaster(2009, 12, 26)).toBe(false);
	});

	it('1.1.2010 is not the Easter', () => {
		expect(isEaster(2010, 1, 1)).toBe(false);
	});

	it('6.1.2010 is not the Easter', () => {
		expect(isEaster(2010, 1, 6)).toBe(false);
	});

	it('2.4.2010 is the Easter', () => {
		expect(isEaster(2010, 4, 2)).toBe(true);
	});

	it('4.4.2010 is the Easter', () => {
		expect(isEaster(2010, 4, 4)).toBe(true);
	});

	it('5.4.2010 is the Easter', () => {
		expect(isEaster(2010, 4, 5)).toBe(true);
	});

	it('1.5.2010 is not the Easter', () => {
		expect(isEaster(2010, 5, 1)).toBe(false);
	});

	it('8.5.2010 is not the Easter', () => {
		expect(isEaster(2010, 5, 8)).toBe(false);
	});

	it('5.7.2010 is not the Easter', () => {
		expect(isEaster(2010, 7, 5)).toBe(false);
	});

	it('29.8.2010 is not the Easter', () => {
		expect(isEaster(2010, 8, 29)).toBe(false);
	});

	it('1.9.2010 is not the Easter', () => {
		expect(isEaster(2010, 9, 1)).toBe(false);
	});

	it('15.9.2010 is not the Easter', () => {
		expect(isEaster(2010, 9, 15)).toBe(false);
	});

	it('1.11.2010 is not the Easter', () => {
		expect(isEaster(2010, 11, 1)).toBe(false);
	});

	it('17.11.2010 is not the Easter', () => {
		expect(isEaster(2010, 11, 17)).toBe(false);
	});

	it('24.12.2010 is not the Easter', () => {
		expect(isEaster(2010, 12, 24)).toBe(false);
	});

	it('25.12.2010 is not the Easter', () => {
		expect(isEaster(2010, 12, 25)).toBe(false);
	});

	it('26.12.2010 is not the Easter', () => {
		expect(isEaster(2010, 12, 26)).toBe(false);
	});

	it('1.1.2011 is not the Easter', () => {
		expect(isEaster(2011, 1, 1)).toBe(false);
	});

	it('6.1.2011 is not the Easter', () => {
		expect(isEaster(2011, 1, 6)).toBe(false);
	});

	it('22.4.2011 is the Easter', () => {
		expect(isEaster(2011, 4, 22)).toBe(true);
	});

	it('24.4.2011 is the Easter', () => {
		expect(isEaster(2011, 4, 25)).toBe(true);
	});

	it('25.4.2011 is the Easter', () => {
		expect(isEaster(2011, 4, 25)).toBe(true);
	});

	it('1.5.2011 is not the Easter', () => {
		expect(isEaster(2011, 5, 1)).toBe(false);
	});

	it('8.5.2011 is not the Easter', () => {
		expect(isEaster(2011, 5, 8)).toBe(false);
	});

	it('5.7.2011 is not the Easter', () => {
		expect(isEaster(2011, 7, 5)).toBe(false);
	});

	it('29.8.2011 is not the Easter', () => {
		expect(isEaster(2011, 8, 29)).toBe(false);
	});

	it('1.9.2011 is not the Easter', () => {
		expect(isEaster(2011, 9, 1)).toBe(false);
	});

	it('15.9.2011 is not the Easter', () => {
		expect(isEaster(2011, 9, 15)).toBe(false);
	});

	it('1.11.2011 is not the Easter', () => {
		expect(isEaster(2011, 11, 1)).toBe(false);
	});

	it('17.11.2011 is not the Easter', () => {
		expect(isEaster(2011, 11, 17)).toBe(false);
	});

	it('24.12.2011 is not the Easter', () => {
		expect(isEaster(2011, 12, 24)).toBe(false);
	});

	it('25.12.2011 is not the Easter', () => {
		expect(isEaster(2011, 12, 25)).toBe(false);
	});

	it('26.12.2011 is not the Easter', () => {
		expect(isEaster(2011, 12, 26)).toBe(false);
	});

	it('1.1.2012 is not the Easter', () => {
		expect(isEaster(2012, 1, 1)).toBe(false);
	});

	it('6.1.2012 is not the Easter', () => {
		expect(isEaster(2012, 1, 6)).toBe(false);
	});

	it('6.4.2012 is the Easter', () => {
		expect(isEaster(2012, 4, 6)).toBe(true);
	});

	it('8.4.2012 is the Easter', () => {
		expect(isEaster(2012, 4, 8)).toBe(true);
	});

	it('9.4.2012 is the Easter', () => {
		expect(isEaster(2012, 4, 9)).toBe(true);
	});

	it('1.5.2012 is not the Easter', () => {
		expect(isEaster(2012, 5, 1)).toBe(false);
	});

	it('8.5.2012 is not the Easter', () => {
		expect(isEaster(2012, 5, 8)).toBe(false);
	});

	it('5.7.2012 is not the Easter', () => {
		expect(isEaster(2012, 7, 5)).toBe(false);
	});

	it('29.8.2012 is not the Easter', () => {
		expect(isEaster(2012, 8, 29)).toBe(false);
	});

	it('1.9.2012 is not the Easter', () => {
		expect(isEaster(2012, 9, 1)).toBe(false);
	});

	it('15.9.2012 is not the Easter', () => {
		expect(isEaster(2012, 9, 15)).toBe(false);
	});

	it('1.11.2012 is not the Easter', () => {
		expect(isEaster(2012, 11, 1)).toBe(false);
	});

	it('17.11.2012 is not the Easter', () => {
		expect(isEaster(2012, 11, 17)).toBe(false);
	});

	it('24.12.2012 is not the Easter', () => {
		expect(isEaster(2012, 12, 24)).toBe(false);
	});

	it('25.12.2012 is not the Easter', () => {
		expect(isEaster(2012, 12, 25)).toBe(false);
	});

	it('26.12.2012 is not the Easter', () => {
		expect(isEaster(2012, 12, 26)).toBe(false);
	});

	it('1.1.2013 is not the Easter', () => {
		expect(isEaster(2013, 1, 1)).toBe(false);
	});

	it('6.1.2013 is not the Easter', () => {
		expect(isEaster(2013, 1, 6)).toBe(false);
	});

	it('29.3.2013 is the Easter', () => {
		expect(isEaster(2013, 3, 29)).toBe(true);
	});

	it('31.3.2013 is the Easter', () => {
		expect(isEaster(2013, 3, 31)).toBe(true);
	});

	it('1.4.2013 is the Easter', () => {
		expect(isEaster(2013, 4, 1)).toBe(true);
	});

	it('1.5.2013 is not the Easter', () => {
		expect(isEaster(2013, 5, 1)).toBe(false);
	});

	it('8.5.2013 is not the Easter', () => {
		expect(isEaster(2013, 5, 8)).toBe(false);
	});

	it('5.7.2013 is not the Easter', () => {
		expect(isEaster(2013, 7, 5)).toBe(false);
	});

	it('29.8.2013 is not the Easter', () => {
		expect(isEaster(2013, 8, 29)).toBe(false);
	});

	it('1.9.2013 is not the Easter', () => {
		expect(isEaster(2013, 9, 1)).toBe(false);
	});

	it('15.9.2013 is not the Easter', () => {
		expect(isEaster(2013, 9, 15)).toBe(false);
	});

	it('1.11.2013 is not the Easter', () => {
		expect(isEaster(2013, 11, 1)).toBe(false);
	});

	it('17.11.2013 is not the Easter', () => {
		expect(isEaster(2013, 11, 17)).toBe(false);
	});

	it('24.12.2013 is not the Easter', () => {
		expect(isEaster(2013, 12, 24)).toBe(false);
	});

	it('25.12.2013 is not the Easter', () => {
		expect(isEaster(2013, 12, 25)).toBe(false);
	});

	it('26.12.2013 is not the Easter', () => {
		expect(isEaster(2013, 12, 26)).toBe(false);
	});

	it('1.1.2014 is not the Easter', () => {
		expect(isEaster(2014, 1, 1)).toBe(false);
	});

	it('6.1.2014 is not the Easter', () => {
		expect(isEaster(2014, 1, 6)).toBe(false);
	});

	it('18.4.2014 is the Easter', () => {
		expect(isEaster(2014, 4, 18)).toBe(true);
	});

	it('20.4.2014 is the Easter', () => {
		expect(isEaster(2014, 4, 20)).toBe(true);
	});

	it('21.4.2014 is the Easter', () => {
		expect(isEaster(2014, 4, 21)).toBe(true);
	});

	it('1.5.2014 is not the Easter', () => {
		expect(isEaster(2014, 5, 1)).toBe(false);
	});

	it('8.5.2014 is not the Easter', () => {
		expect(isEaster(2014, 5, 8)).toBe(false);
	});

	it('5.7.2014 is not the Easter', () => {
		expect(isEaster(2014, 7, 5)).toBe(false);
	});

	it('29.8.2014 is not the Easter', () => {
		expect(isEaster(2014, 8, 29)).toBe(false);
	});

	it('1.9.2014 is not the Easter', () => {
		expect(isEaster(2014, 9, 1)).toBe(false);
	});

	it('15.9.2014 is not the Easter', () => {
		expect(isEaster(2014, 9, 15)).toBe(false);
	});

	it('1.11.2014 is not the Easter', () => {
		expect(isEaster(2014, 11, 1)).toBe(false);
	});

	it('17.11.2014 is not the Easter', () => {
		expect(isEaster(2014, 11, 17)).toBe(false);
	});

	it('24.12.2014 is not the Easter', () => {
		expect(isEaster(2014, 12, 24)).toBe(false);
	});

	it('25.12.2014 is not the Easter', () => {
		expect(isEaster(2014, 12, 25)).toBe(false);
	});

	it('26.12.2014 is not the Easter', () => {
		expect(isEaster(2014, 12, 26)).toBe(false);
	});

	it('1.1.2015 is not the Easter', () => {
		expect(isEaster(2015, 1, 1)).toBe(false);
	});

	it('6.1.2015 is not the Easter', () => {
		expect(isEaster(2015, 1, 6)).toBe(false);
	});

	it('3.4.2015 is the Easter', () => {
		expect(isEaster(2015, 4, 3)).toBe(true);
	});

	it('5.4.2015 is the Easter', () => {
		expect(isEaster(2015, 4, 5)).toBe(true);
	});

	it('6.4.2015 is the Easter', () => {
		expect(isEaster(2015, 4, 6)).toBe(true);
	});

	it('1.5.2015 is not the Easter', () => {
		expect(isEaster(2015, 5, 1)).toBe(false);
	});

	it('8.5.2015 is not the Easter', () => {
		expect(isEaster(2015, 5, 8)).toBe(false);
	});

	it('5.7.2015 is not the Easter', () => {
		expect(isEaster(2015, 7, 5)).toBe(false);
	});

	it('29.8.2015 is not the Easter', () => {
		expect(isEaster(2015, 8, 29)).toBe(false);
	});

	it('1.9.2015 is not the Easter', () => {
		expect(isEaster(2015, 9, 1)).toBe(false);
	});

	it('15.9.2015 is not the Easter', () => {
		expect(isEaster(2015, 9, 15)).toBe(false);
	});

	it('1.11.2015 is not the Easter', () => {
		expect(isEaster(2015, 11, 1)).toBe(false);
	});

	it('17.11.2015 is not the Easter', () => {
		expect(isEaster(2015, 11, 17)).toBe(false);
	});

	it('24.12.2015 is not the Easter', () => {
		expect(isEaster(2015, 12, 24)).toBe(false);
	});

	it('25.12.2015 is not the Easter', () => {
		expect(isEaster(2015, 12, 25)).toBe(false);
	});

	it('26.12.2015 is not the Easter', () => {
		expect(isEaster(2015, 12, 26)).toBe(false);
	});

	it('1.1.2016 is not the Easter', () => {
		expect(isEaster(2016, 1, 1)).toBe(false);
	});

	it('6.1.2016 is not the Easter', () => {
		expect(isEaster(2016, 1, 6)).toBe(false);
	});

	it('25.3.2016 is the Easter', () => {
		expect(isEaster(2016, 3, 25)).toBe(true);
	});

	it('27.3.2016 is the Easter', () => {
		expect(isEaster(2016, 3, 27)).toBe(true);
	});

	it('28.3.2016 is the Easter', () => {
		expect(isEaster(2016, 3, 28)).toBe(true);
	});

	it('1.5.2016 is not the Easter', () => {
		expect(isEaster(2016, 5, 1)).toBe(false);
	});

	it('8.5.2016 is not the Easter', () => {
		expect(isEaster(2016, 5, 8)).toBe(false);
	});

	it('5.7.2016 is not the Easter', () => {
		expect(isEaster(2016, 7, 5)).toBe(false);
	});

	it('29.8.2016 is not the Easter', () => {
		expect(isEaster(2016, 8, 29)).toBe(false);
	});

	it('1.9.2016 is not the Easter', () => {
		expect(isEaster(2016, 9, 1)).toBe(false);
	});

	it('15.9.2016 is not the Easter', () => {
		expect(isEaster(2016, 9, 15)).toBe(false);
	});

	it('1.11.2016 is not the Easter', () => {
		expect(isEaster(2016, 11, 1)).toBe(false);
	});

	it('17.11.2016 is not the Easter', () => {
		expect(isEaster(2016, 11, 17)).toBe(false);
	});

	it('24.12.2016 is not the Easter', () => {
		expect(isEaster(2016, 12, 24)).toBe(false);
	});

	it('25.12.2016 is not the Easter', () => {
		expect(isEaster(2016, 12, 25)).toBe(false);
	});

	it('26.12.2016 is not the Easter', () => {
		expect(isEaster(2016, 12, 26)).toBe(false);
	});

	it('1.1.2017 is not the Easter', () => {
		expect(isEaster(2017, 1, 1)).toBe(false);
	});

	it('6.1.2017 is not the Easter', () => {
		expect(isEaster(2017, 1, 6)).toBe(false);
	});

	it('14.4.2017 is the Easter', () => {
		expect(isEaster(2017, 4, 14)).toBe(true);
	});

	it('16.4.2017 is the Easter', () => {
		expect(isEaster(2017, 4, 16)).toBe(true);
	});

	it('17.4.2017 is the Easter', () => {
		expect(isEaster(2017, 4, 17)).toBe(true);
	});

	it('1.5.2017 is not the Easter', () => {
		expect(isEaster(2017, 5, 1)).toBe(false);
	});

	it('8.5.2017 is not the Easter', () => {
		expect(isEaster(2017, 5, 8)).toBe(false);
	});

	it('5.7.2017 is not the Easter', () => {
		expect(isEaster(2017, 7, 5)).toBe(false);
	});

	it('29.8.2017 is not the Easter', () => {
		expect(isEaster(2017, 8, 29)).toBe(false);
	});

	it('1.9.2017 is not the Easter', () => {
		expect(isEaster(2017, 9, 1)).toBe(false);
	});

	it('15.9.2017 is not the Easter', () => {
		expect(isEaster(2017, 9, 15)).toBe(false);
	});

	it('1.11.2017 is not the Easter', () => {
		expect(isEaster(2017, 11, 1)).toBe(false);
	});

	it('17.11.2017 is not the Easter', () => {
		expect(isEaster(2017, 11, 17)).toBe(false);
	});

	it('24.12.2017 is not the Easter', () => {
		expect(isEaster(2017, 12, 24)).toBe(false);
	});

	it('25.12.2017 is not the Easter', () => {
		expect(isEaster(2017, 12, 25)).toBe(false);
	});

	it('26.12.2017 is not the Easter', () => {
		expect(isEaster(2017, 12, 26)).toBe(false);
	});

	it('1.1.2018 is not the Easter', () => {
		expect(isEaster(2018, 1, 1)).toBe(false);
	});

	it('6.1.2018 is not the Easter', () => {
		expect(isEaster(2018, 1, 6)).toBe(false);
	});

	it('30.3.2018 is the Easter', () => {
		expect(isEaster(2018, 3, 30)).toBe(true);
	});

	it('1.4.2018 is the Easter', () => {
		expect(isEaster(2018, 4, 1)).toBe(true);
	});

	it('2.4.2018 is the Easter', () => {
		expect(isEaster(2018, 4, 2)).toBe(true);
	});

	it('1.5.2018 is not the Easter', () => {
		expect(isEaster(2018, 5, 1)).toBe(false);
	});

	it('8.5.2018 is not the Easter', () => {
		expect(isEaster(2018, 5, 8)).toBe(false);
	});

	it('5.7.2018 is not the Easter', () => {
		expect(isEaster(2018, 7, 5)).toBe(false);
	});

	it('29.8.2018 is not the Easter', () => {
		expect(isEaster(2018, 8, 29)).toBe(false);
	});

	it('1.9.2018 is not the Easter', () => {
		expect(isEaster(2018, 9, 1)).toBe(false);
	});

	it('15.9.2018 is not the Easter', () => {
		expect(isEaster(2018, 9, 15)).toBe(false);
	});

	it('30.10.2018 is not the Easter', () => {
		expect(isEaster(2018, 10, 30)).toBe(false);
	});

	it('1.11.2018 is not the Easter', () => {
		expect(isEaster(2018, 11, 1)).toBe(false);
	});

	it('17.11.2018 is not the Easter', () => {
		expect(isEaster(2018, 11, 17)).toBe(false);
	});

	it('24.12.2018 is not the Easter', () => {
		expect(isEaster(2018, 12, 24)).toBe(false);
	});

	it('25.12.2018 is not the Easter', () => {
		expect(isEaster(2018, 12, 25)).toBe(false);
	});

	it('26.12.2018 is not the Easter', () => {
		expect(isEaster(2018, 12, 26)).toBe(false);
	});

	it('1.1.2019 is not the Easter', () => {
		expect(isEaster(2019, 1, 1)).toBe(false);
	});

	it('6.1.2019 is not the Easter', () => {
		expect(isEaster(2019, 1, 6)).toBe(false);
	});

	it('19.4.2019 is the Easter', () => {
		expect(isEaster(2019, 4, 19)).toBe(true);
	});

	it('21.4.2019 is the Easter', () => {
		expect(isEaster(2019, 4, 21)).toBe(true);
	});

	it('22.4.2019 is the Easter', () => {
		expect(isEaster(2019, 4, 22)).toBe(true);
	});

	it('1.5.2019 is not the Easter', () => {
		expect(isEaster(2019, 5, 1)).toBe(false);
	});

	it('8.5.2019 is not the Easter', () => {
		expect(isEaster(2019, 5, 8)).toBe(false);
	});

	it('5.7.2019 is not the Easter', () => {
		expect(isEaster(2019, 7, 5)).toBe(false);
	});

	it('29.8.2019 is not the Easter', () => {
		expect(isEaster(2019, 8, 29)).toBe(false);
	});

	it('1.9.2019 is not the Easter', () => {
		expect(isEaster(2019, 9, 1)).toBe(false);
	});

	it('15.9.2019 is not the Easter', () => {
		expect(isEaster(2019, 9, 15)).toBe(false);
	});

	it('1.11.2019 is not the Easter', () => {
		expect(isEaster(2019, 11, 1)).toBe(false);
	});

	it('17.11.2019 is not the Easter', () => {
		expect(isEaster(2019, 11, 17)).toBe(false);
	});

	it('24.12.2019 is not the Easter', () => {
		expect(isEaster(2019, 12, 24)).toBe(false);
	});

	it('25.12.2019 is not the Easter', () => {
		expect(isEaster(2019, 12, 25)).toBe(false);
	});

	it('26.12.2019 is not the Easter', () => {
		expect(isEaster(2019, 12, 26)).toBe(false);
	});

	it('1.1.2020 is not the Easter', () => {
		expect(isEaster(2020, 1, 1)).toBe(false);
	});

	it('6.1.2020 is not the Easter', () => {
		expect(isEaster(2020, 1, 6)).toBe(false);
	});

	it('10.4.2020 is the Easter', () => {
		expect(isEaster(2020, 4, 10)).toBe(true);
	});

	it('12.4.2020 is the Easter', () => {
		expect(isEaster(2020, 4, 12)).toBe(true);
	});

	it('13.4.2020 is the Easter', () => {
		expect(isEaster(2020, 4, 13)).toBe(true);
	});

	it('1.5.2020 is not the Easter', () => {
		expect(isEaster(2020, 5, 1)).toBe(false);
	});

	it('8.5.2020 is not the Easter', () => {
		expect(isEaster(2020, 5, 8)).toBe(false);
	});

	it('5.7.2020 is not the Easter', () => {
		expect(isEaster(2020, 7, 5)).toBe(false);
	});

	it('29.8.2020 is not the Easter', () => {
		expect(isEaster(2020, 8, 29)).toBe(false);
	});

	it('1.9.2020 is not the Easter', () => {
		expect(isEaster(2020, 9, 1)).toBe(false);
	});

	it('15.9.2020 is not the Easter', () => {
		expect(isEaster(2020, 9, 15)).toBe(false);
	});

	it('1.11.2020 is not the Easter', () => {
		expect(isEaster(2020, 11, 1)).toBe(false);
	});

	it('17.11.2020 is not the Easter', () => {
		expect(isEaster(2020, 11, 17)).toBe(false);
	});

	it('24.12.2020 is not the Easter', () => {
		expect(isEaster(2020, 12, 24)).toBe(false);
	});

	it('25.12.2020 is not the Easter', () => {
		expect(isEaster(2020, 12, 25)).toBe(false);
	});

	it('26.12.2020 is not the Easter', () => {
		expect(isEaster(2020, 12, 26)).toBe(false);
	});

	it('1.1.2021 is not the Easter', () => {
		expect(isEaster(2021, 1, 1)).toBe(false);
	});

	it('6.1.2021 is not the Easter', () => {
		expect(isEaster(2021, 1, 6)).toBe(false);
	});

	it('2.4.2021 is the Easter', () => {
		expect(isEaster(2021, 4, 2)).toBe(true);
	});

	it('4.4.2021 is the Easter', () => {
		expect(isEaster(2021, 4, 4)).toBe(true);
	});

	it('5.4.2021 is the Easter', () => {
		expect(isEaster(2021, 4, 5)).toBe(true);
	});

	it('1.5.2021 is not the Easter', () => {
		expect(isEaster(2021, 5, 1)).toBe(false);
	});

	it('8.5.2021 is not the Easter', () => {
		expect(isEaster(2021, 5, 8)).toBe(false);
	});

	it('5.7.2021 is not the Easter', () => {
		expect(isEaster(2021, 7, 5)).toBe(false);
	});

	it('29.8.2021 is not the Easter', () => {
		expect(isEaster(2021, 8, 29)).toBe(false);
	});

	it('1.9.2021 is not the Easter', () => {
		expect(isEaster(2021, 9, 1)).toBe(false);
	});

	it('15.9.2021 is not the Easter', () => {
		expect(isEaster(2021, 9, 15)).toBe(false);
	});

	it('1.11.2021 is not the Easter', () => {
		expect(isEaster(2021, 11, 1)).toBe(false);
	});

	it('17.11.2021 is not the Easter', () => {
		expect(isEaster(2021, 11, 17)).toBe(false);
	});

	it('24.12.2021 is not the Easter', () => {
		expect(isEaster(2021, 12, 24)).toBe(false);
	});

	it('25.12.2021 is not the Easter', () => {
		expect(isEaster(2021, 12, 25)).toBe(false);
	});

	it('26.12.2021 is not the Easter', () => {
		expect(isEaster(2021, 12, 26)).toBe(false);
	});

	it('1.1.2022 is not the Easter', () => {
		expect(isEaster(2022, 1, 1)).toBe(false);
	});

	it('6.1.2022 is not the Easter', () => {
		expect(isEaster(2022, 1, 6)).toBe(false);
	});

	it('15.4.2022 is the Easter', () => {
		expect(isEaster(2022, 4, 15)).toBe(true);
	});

	it('17.4.2022 is the Easter', () => {
		expect(isEaster(2022, 4, 17)).toBe(true);
	});

	it('18.4.2022 is the Easter', () => {
		expect(isEaster(2022, 4, 18)).toBe(true);
	});

	it('1.5.2022 is not the Easter', () => {
		expect(isEaster(2022, 5, 1)).toBe(false);
	});

	it('8.5.2022 is not the Easter', () => {
		expect(isEaster(2022, 5, 8)).toBe(false);
	});

	it('5.7.2022 is not the Easter', () => {
		expect(isEaster(2022, 7, 5)).toBe(false);
	});

	it('29.8.2022 is not the Easter', () => {
		expect(isEaster(2022, 8, 29)).toBe(false);
	});

	it('1.9.2022 is not the Easter', () => {
		expect(isEaster(2022, 9, 1)).toBe(false);
	});

	it('15.9.2022 is not the Easter', () => {
		expect(isEaster(2022, 9, 15)).toBe(false);
	});

	it('1.11.2022 is not the Easter', () => {
		expect(isEaster(2022, 11, 1)).toBe(false);
	});

	it('17.11.2022 is not the Easter', () => {
		expect(isEaster(2022, 11, 17)).toBe(false);
	});

	it('24.12.2022 is not the Easter', () => {
		expect(isEaster(2022, 12, 24)).toBe(false);
	});

	it('25.12.2022 is not the Easter', () => {
		expect(isEaster(2022, 12, 25)).toBe(false);
	});

	it('26.12.2022 is not the Easter', () => {
		expect(isEaster(2022, 12, 26)).toBe(false);
	});

	it('1.1.2023 is not the Easter', () => {
		expect(isEaster(2023, 1, 1)).toBe(false);
	});

	it('6.1.2023 is not the Easter', () => {
		expect(isEaster(2023, 1, 6)).toBe(false);
	});

	it('7.4.2023 is the Easter', () => {
		expect(isEaster(2023, 4, 7)).toBe(true);
	});

	it('9.4.2023 is the Easter', () => {
		expect(isEaster(2023, 4, 9)).toBe(true);
	});

	it('10.4.2023 is the Easter', () => {
		expect(isEaster(2023, 4, 10)).toBe(true);
	});

	it('1.5.2023 is not the Easter', () => {
		expect(isEaster(2023, 5, 1)).toBe(false);
	});

	it('8.5.2023 is not the Easter', () => {
		expect(isEaster(2023, 5, 8)).toBe(false);
	});

	it('5.7.2023 is not the Easter', () => {
		expect(isEaster(2023, 7, 5)).toBe(false);
	});

	it('29.8.2023 is not the Easter', () => {
		expect(isEaster(2023, 8, 29)).toBe(false);
	});

	it('1.9.2023 is not the Easter', () => {
		expect(isEaster(2023, 9, 1)).toBe(false);
	});

	it('15.9.2023 is not the Easter', () => {
		expect(isEaster(2023, 9, 15)).toBe(false);
	});

	it('1.11.2023 is not the Easter', () => {
		expect(isEaster(2023, 11, 1)).toBe(false);
	});

	it('17.11.2023 is not the Easter', () => {
		expect(isEaster(2023, 11, 17)).toBe(false);
	});

	it('24.12.2023 is not the Easter', () => {
		expect(isEaster(2023, 12, 24)).toBe(false);
	});

	it('25.12.2023 is not the Easter', () => {
		expect(isEaster(2023, 12, 25)).toBe(false);
	});

	it('26.12.2023 is not the Easter', () => {
		expect(isEaster(2023, 12, 26)).toBe(false);
	});

	it('1.1.1992 is not the Easter', () => {
		expect(isEaster(1992, 1, 1)).toBe(false);
	});

	it('6.1.1992 is not the Easter', () => {
		expect(isEaster(1992, 1, 6)).toBe(false);
	});

	it('9.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 9)).toBe(false);
	});

	it('11.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 11)).toBe(false);
	});

	it('12.4.1992 is not the Easter', () => {
		expect(isEaster(1992, 4, 12)).toBe(false);
	});

	it('1.5.1992 is not the Easter', () => {
		expect(isEaster(1992, 5, 1)).toBe(false);
	});

	it('8.5.1992 is not the Easter', () => {
		expect(isEaster(1992, 5, 8)).toBe(false);
	});

	it('5.7.1992 is not the Easter', () => {
		expect(isEaster(1992, 7, 5)).toBe(false);
	});

	it('29.8.1992 is not the Easter', () => {
		expect(isEaster(1992, 8, 29)).toBe(false);
	});

	it('1.9.1992 is not the Easter', () => {
		expect(isEaster(1992, 9, 1)).toBe(false);
	});

	it('15.9.1992 is not the Easter', () => {
		expect(isEaster(1992, 9, 15)).toBe(false);
	});

	it('1.11.1992 is not the Easter', () => {
		expect(isEaster(1992, 11, 1)).toBe(false);
	});

	it('17.11.1992 is not the Easter', () => {
		expect(isEaster(1992, 11, 17)).toBe(false);
	});

	it('24.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 24)).toBe(false);
	});

	it('25.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 25)).toBe(false);
	});

	it('26.12.1992 is not the Easter', () => {
		expect(isEaster(1992, 12, 26)).toBe(false);
	});

	it('2.1.1993 is not the Easter', () => {
		expect(isEaster(1993, 1, 2)).toBe(false);
	});

	it('5.1.1993 is not the Easter', () => {
		expect(isEaster(1993, 1, 5)).toBe(false);
	});

	it('8.4.1993 is not the Easter', () => {
		expect(isEaster(1993, 4, 8)).toBe(false);
	});

	it('10.4.1993 is not the Easter', () => {
		expect(isEaster(1993, 4, 10)).toBe(false);
	});

	it('13.4.1993 is not the Easter', () => {
		expect(isEaster(1993, 4, 13)).toBe(false);
	});

	it('2.5.1993 is not the Easter', () => {
		expect(isEaster(1993, 5, 2)).toBe(false);
	});

	it('7.5.1993 is not the Easter', () => {
		expect(isEaster(1993, 5, 7)).toBe(false);
	});

	it('3.7.1993 is not the Easter', () => {
		expect(isEaster(1993, 7, 3)).toBe(false);
	});

	it('26.8.1993 is not the Easter', () => {
		expect(isEaster(1993, 8, 26)).toBe(false);
	});

	it('11.9.1993 is not the Easter', () => {
		expect(isEaster(1993, 9, 11)).toBe(false);
	});

	it('12.9.1993 is not the Easter', () => {
		expect(isEaster(1993, 9, 12)).toBe(false);
	});

	it('30.10.1993 is not the Easter', () => {
		expect(isEaster(1993, 10, 30)).toBe(false);
	});

	it('12.11.1993 is not the Easter', () => {
		expect(isEaster(1993, 11, 12)).toBe(false);
	});

	it('19.11.1993 is not the Easter', () => {
		expect(isEaster(1993, 11, 19)).toBe(false);
	});

	it('20.12.1993 is not the Easter', () => {
		expect(isEaster(1993, 12, 20)).toBe(false);
	});

	it('12.1.1994 is not the Easter', () => {
		expect(isEaster(1994, 1, 12)).toBe(false);
	});

	it('8.1.1994 is not the Easter', () => {
		expect(isEaster(1994, 1, 8)).toBe(false);
	});

	it('11.4.1994 is not the Easter', () => {
		expect(isEaster(1994, 4, 11)).toBe(false);
	});

	it('30.4.1994 is not the Easter', () => {
		expect(isEaster(1994, 4, 30)).toBe(false);
	});

	it('10.5.1994 is not the Easter', () => {
		expect(isEaster(1994, 5, 10)).toBe(false);
	});

	it('18.5.1994 is not the Easter', () => {
		expect(isEaster(1994, 5, 18)).toBe(false);
	});

	it('15.7.1994 is not the Easter', () => {
		expect(isEaster(1994, 7, 15)).toBe(false);
	});

	it('21.8.1994 is not the Easter', () => {
		expect(isEaster(1994, 8, 21)).toBe(false);
	});

	it('3.9.1994 is not the Easter', () => {
		expect(isEaster(1994, 9, 3)).toBe(false);
	});

	it('10.9.1994 is not the Easter', () => {
		expect(isEaster(1994, 9, 10)).toBe(false);
	});

	it('30.10.1994 is not the Easter', () => {
		expect(isEaster(1994, 10, 30)).toBe(false);
	});

	it('13.11.1994 is not the Easter', () => {
		expect(isEaster(1994, 11, 13)).toBe(false);
	});

	it('20.11.1994 is not the Easter', () => {
		expect(isEaster(1994, 11, 20)).toBe(false);
	});

	it('3.1.1995 is not the Easter', () => {
		expect(isEaster(1995, 1, 3)).toBe(false);
	});

	it('10.1.1995 is not the Easter', () => {
		expect(isEaster(1995, 1, 10)).toBe(false);
	});

	it('12.4.1995 is not the Easter', () => {
		expect(isEaster(1995, 4, 12)).toBe(false);
	});

	it('3.5.1995 is not the Easter', () => {
		expect(isEaster(1995, 5, 3)).toBe(false);
	});

	it('7.5.1995 is not the Easter', () => {
		expect(isEaster(1995, 5, 7)).toBe(false);
	});

	it('6.7.1995 is not the Easter', () => {
		expect(isEaster(1995, 7, 6)).toBe(false);
	});

	it('28.8.1995 is not the Easter', () => {
		expect(isEaster(1995, 8, 28)).toBe(false);
	});

	it('2.9.1995 is not the Easter', () => {
		expect(isEaster(1995, 9, 2)).toBe(false);
	});

	it('16.9.1995 is not the Easter', () => {
		expect(isEaster(1995, 9, 16)).toBe(false);
	});

	it('30.10.1995 is not the Easter', () => {
		expect(isEaster(1995, 10, 30)).toBe(false);
	});

	it('12.11.1995 is not the Easter', () => {
		expect(isEaster(1995, 11, 12)).toBe(false);
	});

	it('28.12.1995 is not the Easter', () => {
		expect(isEaster(1995, 12, 28)).toBe(false);
	});

	it('2.1.1996 is not the Easter', () => {
		expect(isEaster(1996, 1, 2)).toBe(false);
	});

	it('7.1.1996 is not the Easter', () => {
		expect(isEaster(1996, 1, 7)).toBe(false);
	});

	it('3.4.1996 is not the Easter', () => {
		expect(isEaster(1996, 4, 3)).toBe(false);
	});

	it('9.5.1996 is not the Easter', () => {
		expect(isEaster(1996, 5, 9)).toBe(false);
	});

	it('4.7.1996 is not the Easter', () => {
		expect(isEaster(1996, 7, 4)).toBe(false);
	});

	it('30.8.1996 is not the Easter', () => {
		expect(isEaster(1996, 8, 30)).toBe(false);
	});

	it('14.9.1996 is not the Easter', () => {
		expect(isEaster(1996, 9, 14)).toBe(false);
	});

	it('30.10.1996 is not the Easter', () => {
		expect(isEaster(1996, 10, 30)).toBe(false);
	});

	it('2.11.1996 is not the Easter', () => {
		expect(isEaster(1996, 11, 2)).toBe(false);
	});

	it('16.11.1996 is not the Easter', () => {
		expect(isEaster(1996, 11, 16)).toBe(false);
	});

	it('31.12.1996 is not the Easter', () => {
		expect(isEaster(1996, 12, 31)).toBe(false);
	});

	it('2.1.1997 is not the Easter', () => {
		expect(isEaster(1997, 1, 2)).toBe(false);
	});

	it('1.4.1997 is not the Easter', () => {
		expect(isEaster(1997, 4, 1)).toBe(false);
	});

	it('5.5.1997 is not the Easter', () => {
		expect(isEaster(1997, 5, 5)).toBe(false);
	});

	it('6.7.1997 is not the Easter', () => {
		expect(isEaster(1997, 7, 6)).toBe(false);
	});

	it('27.8.1997 is not the Easter', () => {
		expect(isEaster(1997, 8, 27)).toBe(false);
	});

	it('4.9.1997 is not the Easter', () => {
		expect(isEaster(1997, 9, 4)).toBe(false);
	});

	it('12.9.1997 is not the Easter', () => {
		expect(isEaster(1997, 9, 12)).toBe(false);
	});

	it('30.10.1997 is not the Easter', () => {
		expect(isEaster(1997, 10, 30)).toBe(false);
	});

	it('16.11.1997 is not the Easter', () => {
		expect(isEaster(1997, 11, 16)).toBe(false);
	});

	it('10.3.1998 is not the Easter', () => {
		expect(isEaster(1998, 3, 10)).toBe(false);
	});

	it('1.6.1998 is not the Easter', () => {
		expect(isEaster(1998, 6, 1)).toBe(false);
	});

	it('27.8.1998 is not the Easter', () => {
		expect(isEaster(1998, 8, 27)).toBe(false);
	});

	it('13.9.1998 is not the Easter', () => {
		expect(isEaster(1998, 9, 13)).toBe(false);
	});

	it('30.10.1998 is not the Easter', () => {
		expect(isEaster(1998, 10, 30)).toBe(false);
	});

	it('15.11.1998 is not the Easter', () => {
		expect(isEaster(1998, 11, 15)).toBe(false);
	});

	it('10.1.1999 is not the Easter', () => {
		expect(isEaster(1999, 1, 10)).toBe(false);
	});

	it('20.4.1999 is not the Easter', () => {
		expect(isEaster(1999, 4, 20)).toBe(false);
	});

	it('10.5.1999 is not the Easter', () => {
		expect(isEaster(1999, 5, 10)).toBe(false);
	});

	it('15.7.1999 is not the Easter', () => {
		expect(isEaster(1999, 7, 15)).toBe(false);
	});

	it('16.9.1999 is not the Easter', () => {
		expect(isEaster(1999, 9, 16)).toBe(false);
	});

	it('30.10.1999 is not the Easter', () => {
		expect(isEaster(1999, 10, 30)).toBe(false);
	});

	it('18.11.1999 is not the Easter', () => {
		expect(isEaster(1999, 11, 18)).toBe(false);
	});

	it('23.12.1999 is not the Easter', () => {
		expect(isEaster(1999, 12, 23)).toBe(false);
	});

	it('27.12.1999 is not the Easter', () => {
		expect(isEaster(1999, 12, 27)).toBe(false);
	});

	it('12.1.2000 is not the Easter', () => {
		expect(isEaster(2000, 1, 12)).toBe(false);
	});

	it('20.4.2000 is not the Easter', () => {
		expect(isEaster(2000, 4, 20)).toBe(false);
	});

	it('22.4.2000 is not the Easter', () => {
		expect(isEaster(2000, 4, 22)).toBe(false);
	});

	it('18.5.2000 is not the Easter', () => {
		expect(isEaster(2000, 5, 18)).toBe(false);
	});

	it('15.7.2000 is not the Easter', () => {
		expect(isEaster(2000, 7, 15)).toBe(false);
	});

	it('20.8.2000 is not the Easter', () => {
		expect(isEaster(2000, 8, 20)).toBe(false);
	});

	it('11.9.2000 is not the Easter', () => {
		expect(isEaster(2000, 9, 11)).toBe(false);
	});

	it('14.9.2000 is not the Easter', () => {
		expect(isEaster(2000, 9, 14)).toBe(false);
	});

	it('30.10.2000 is not the Easter', () => {
		expect(isEaster(2000, 10, 30)).toBe(false);
	});

	it('3.11.2000 is not the Easter', () => {
		expect(isEaster(2000, 11, 3)).toBe(false);
	});

	it('16.11.2000 is not the Easter', () => {
		expect(isEaster(2000, 11, 16)).toBe(false);
	});

	it('12.1.2001 is not the Easter', () => {
		expect(isEaster(2001, 1, 12)).toBe(false);
	});

	it('13.3.2001 is not the Easter', () => {
		expect(isEaster(2001, 3, 13)).toBe(false);
	});

	it('15.3.2001 is not the Easter', () => {
		expect(isEaster(2001, 3, 15)).toBe(false);
	});

	it('16.3.2001 is not the Easter', () => {
		expect(isEaster(2001, 3, 16)).toBe(false);
	});

	it('1.7.2001 is not the Easter', () => {
		expect(isEaster(2001, 7, 1)).toBe(false);
	});

	it('2.8.2001 is not the Easter', () => {
		expect(isEaster(2001, 8, 2)).toBe(false);
	});

	it('1.10.2001 is not the Easter', () => {
		expect(isEaster(2001, 10, 1)).toBe(false);
	});

	it('30.10.2001 is not the Easter', () => {
		expect(isEaster(2001, 10, 30)).toBe(false);
	});

	it('15.11.2001 is not the Easter', () => {
		expect(isEaster(2001, 11, 15)).toBe(false);
	});

	it('5.12.2001 is not the Easter', () => {
		expect(isEaster(2001, 12, 5)).toBe(false);
	});

	it('12.1.2002 is not the Easter', () => {
		expect(isEaster(2002, 1, 12)).toBe(false);
	});

	it('28.3.2002 is not the Easter', () => {
		expect(isEaster(2002, 3, 28)).toBe(false);
	});

	it('2.4.2002 is not the Easter', () => {
		expect(isEaster(2002, 4, 2)).toBe(false);
	});

	it('2.5.2002 is not the Easter', () => {
		expect(isEaster(2002, 5, 2)).toBe(false);
	});

	it('1.7.2002 is not the Easter', () => {
		expect(isEaster(2002, 7, 1)).toBe(false);
	});

	it('25.8.2002 is not the Easter', () => {
		expect(isEaster(2002, 8, 25)).toBe(false);
	});

	it('21.9.2002 is not the Easter', () => {
		expect(isEaster(2002, 9, 21)).toBe(false);
	});

	it('30.10.2002 is not the Easter', () => {
		expect(isEaster(2002, 10, 30)).toBe(false);
	});

	it('21.11.2002 is not the Easter', () => {
		expect(isEaster(2002, 11, 21)).toBe(false);
	});

	it('22.4.2003 is not the Easter', () => {
		expect(isEaster(2003, 4, 22)).toBe(false);
	});

	it('5.5.2003 is not the Easter', () => {
		expect(isEaster(2003, 5, 5)).toBe(false);
	});

	it('29.7.2003 is not the Easter', () => {
		expect(isEaster(2003, 7, 29)).toBe(false);
	});

	it('1.8.2003 is not the Easter', () => {
		expect(isEaster(2003, 8, 1)).toBe(false);
	});

	it('12.9.2003 is not the Easter', () => {
		expect(isEaster(2003, 9, 12)).toBe(false);
	});

	it('1.12.2003 is not the Easter', () => {
		expect(isEaster(2003, 12, 1)).toBe(false);
	});

	it('11.1.2004 is not the Easter', () => {
		expect(isEaster(2004, 1, 11)).toBe(false);
	});

	it('4.4.2004 is not the Easter', () => {
		expect(isEaster(2004, 4, 4)).toBe(false);
	});

	it('6.5.2004 is not the Easter', () => {
		expect(isEaster(2004, 5, 6)).toBe(false);
	});

	it('6.7.2004 is not the Easter', () => {
		expect(isEaster(2004, 7, 6)).toBe(false);
	});

	it('10.9.2004 is not the Easter', () => {
		expect(isEaster(2004, 9, 10)).toBe(false);
	});

	it('10.11.2004 is not the Easter', () => {
		expect(isEaster(2004, 11, 10)).toBe(false);
	});

	it('12.1.2005 is not the Easter', () => {
		expect(isEaster(2005, 1, 12)).toBe(false);
	});

	it('31.3.2005 is not the Easter', () => {
		expect(isEaster(2005, 3, 31)).toBe(false);
	});

	it('31.5.2005 is not the Easter', () => {
		expect(isEaster(2005, 5, 31)).toBe(false);
	});

	it('31.7.2005 is not the Easter', () => {
		expect(isEaster(2005, 7, 31)).toBe(false);
	});

	it('21.9.2005 is not the Easter', () => {
		expect(isEaster(2005, 9, 21)).toBe(false);
	});

	it('12.11.2005 is not the Easter', () => {
		expect(isEaster(2005, 11, 12)).toBe(false);
	});

	it('16.1.2006 is not the Easter', () => {
		expect(isEaster(2006, 1, 16)).toBe(false);
	});

	it('14.2.2006 is not the Easter', () => {
		expect(isEaster(2006, 2, 14)).toBe(false);
	});

	it('16.2.2006 is not the Easter', () => {
		expect(isEaster(2006, 2, 16)).toBe(false);
	});

	it('17.2.2006 is not the Easter', () => {
		expect(isEaster(2006, 2, 17)).toBe(false);
	});

	it('20.6.2006 is not the Easter', () => {
		expect(isEaster(2006, 6, 20)).toBe(false);
	});

	it('9.3.2007 is not the Easter', () => {
		expect(isEaster(2007, 3, 9)).toBe(false);
	});

	it('1.6.2007 is not the Easter', () => {
		expect(isEaster(2007, 6, 1)).toBe(false);
	});

	it('8.6.2007 is not the Easter', () => {
		expect(isEaster(2007, 6, 8)).toBe(false);
	});

	it('5.8.2007 is not the Easter', () => {
		expect(isEaster(2007, 8, 5)).toBe(false);
	});

	it('1.10.2007 is not the Easter', () => {
		expect(isEaster(2007, 10, 1)).toBe(false);
	});

	it('30.10.2007 is not the Easter', () => {
		expect(isEaster(2007, 10, 30)).toBe(false);
	});

	it('17.12.2007 is not the Easter', () => {
		expect(isEaster(2007, 12, 17)).toBe(false);
	});

	it('7.1.2008 is not the Easter', () => {
		expect(isEaster(2008, 1, 7)).toBe(false);
	});

	it('21.2.2008 is not the Easter', () => {
		expect(isEaster(2008, 2, 21)).toBe(false);
	});

	it('25.3.2008 is not the Easter', () => {
		expect(isEaster(2008, 3, 25)).toBe(false);
	});

	it('2.5.2008 is not the Easter', () => {
		expect(isEaster(2008, 5, 2)).toBe(false);
	});

	it('5.6.2008 is not the Easter', () => {
		expect(isEaster(2008, 6, 5)).toBe(false);
	});

	it('2.8.2008 is not the Easter', () => {
		expect(isEaster(2008, 8, 2)).toBe(false);
	});

	it('2.9.2008 is not the Easter', () => {
		expect(isEaster(2008, 9, 2)).toBe(false);
	});

	it('30.10.2008 is not the Easter', () => {
		expect(isEaster(2008, 10, 30)).toBe(false);
	});

	it('12.11.2008 is not the Easter', () => {
		expect(isEaster(2008, 11, 12)).toBe(false);
	});

	it('13.1.2009 is not the Easter', () => {
		expect(isEaster(2009, 1, 13)).toBe(false);
	});

	it('1.6.2009 is not the Easter', () => {
		expect(isEaster(2009, 6, 1)).toBe(false);
	});

	it('8.7.2009 is not the Easter', () => {
		expect(isEaster(2009, 7, 8)).toBe(false);
	});

	it('25.9.2009 is not the Easter', () => {
		expect(isEaster(2009, 9, 25)).toBe(false);
	});

	it('28.12.2009 is not the Easter', () => {
		expect(isEaster(2009, 12, 28)).toBe(false);
	});

	it('1.2.2010 is not the Easter', () => {
		expect(isEaster(2010, 2, 1)).toBe(false);
	});

	it('1.4.2010 is not the Easter', () => {
		expect(isEaster(2010, 4, 1)).toBe(false);
	});

	it('18.5.2010 is not the Easter', () => {
		expect(isEaster(2010, 5, 18)).toBe(false);
	});

	it('15.7.2010 is not the Easter', () => {
		expect(isEaster(2010, 7, 15)).toBe(false);
	});

	it('2.8.2010 is not the Easter', () => {
		expect(isEaster(2010, 8, 2)).toBe(false);
	});

	it('12.9.2010 is not the Easter', () => {
		expect(isEaster(2010, 9, 12)).toBe(false);
	});

	it('25.9.2010 is not the Easter', () => {
		expect(isEaster(2010, 9, 25)).toBe(false);
	});

	it('30.10.2010 is not the Easter', () => {
		expect(isEaster(2010, 10, 30)).toBe(false);
	});

	it('14.11.2010 is not the Easter', () => {
		expect(isEaster(2010, 11, 14)).toBe(false);
	});

	it('2.1.2011 is not the Easter', () => {
		expect(isEaster(2011, 1, 2)).toBe(false);
	});

	it('26.4.2011 is not the Easter', () => {
		expect(isEaster(2011, 4, 26)).toBe(false);
	});

	it('12.5.2011 is not the Easter', () => {
		expect(isEaster(2011, 5, 12)).toBe(false);
	});

	it('18.5.2011 is not the Easter', () => {
		expect(isEaster(2011, 5, 18)).toBe(false);
	});

	it('20.12.2011 is not the Easter', () => {
		expect(isEaster(2011, 12, 20)).toBe(false);
	});

	it('16.1.2012 is not the Easter', () => {
		expect(isEaster(2012, 1, 16)).toBe(false);
	});

	it('6.2.2012 is not the Easter', () => {
		expect(isEaster(2012, 2, 6)).toBe(false);
	});

	it('8.3.2012 is not the Easter', () => {
		expect(isEaster(2012, 3, 8)).toBe(false);
	});

	it('9.6.2012 is not the Easter', () => {
		expect(isEaster(2012, 6, 9)).toBe(false);
	});

	it('19.8.2012 is not the Easter', () => {
		expect(isEaster(2012, 8, 19)).toBe(false);
	});

	it('19.9.2012 is not the Easter', () => {
		expect(isEaster(2012, 9, 19)).toBe(false);
	});

	it('16.1.2013 is not the Easter', () => {
		expect(isEaster(2013, 1, 16)).toBe(false);
	});

	it('28.2.2013 is not the Easter', () => {
		expect(isEaster(2013, 2, 28)).toBe(false);
	});

	it('1.10.2013 is not the Easter', () => {
		expect(isEaster(2013, 10, 1)).toBe(false);
	});

	it('30.10.2013 is not the Easter', () => {
		expect(isEaster(2013, 10, 30)).toBe(false);
	});

	it('23.12.2013 is not the Easter', () => {
		expect(isEaster(2013, 12, 23)).toBe(false);
	});

	it('27.12.2013 is not the Easter', () => {
		expect(isEaster(2013, 12, 27)).toBe(false);
	});

	it('6.2.2014 is not the Easter', () => {
		expect(isEaster(2014, 2, 6)).toBe(false);
	});

	it('18.3.2014 is not the Easter', () => {
		expect(isEaster(2014, 3, 18)).toBe(false);
	});

	it('8.6.2014 is not the Easter', () => {
		expect(isEaster(2014, 6, 8)).toBe(false);
	});

	it('5.8.2014 is not the Easter', () => {
		expect(isEaster(2014, 8, 5)).toBe(false);
	});

	it('15.10.2014 is not the Easter', () => {
		expect(isEaster(2014, 10, 15)).toBe(false);
	});

	it('30.10.2014 is not the Easter', () => {
		expect(isEaster(2014, 10, 30)).toBe(false);
	});

	it('1.12.2014 is not the Easter', () => {
		expect(isEaster(2014, 12, 1)).toBe(false);
	});

	it('7.1.2015 is not the Easter', () => {
		expect(isEaster(2015, 1, 7)).toBe(false);
	});

	it('3.2.2015 is not the Easter', () => {
		expect(isEaster(2015, 2, 3)).toBe(false);
	});

	it('5.3.2015 is not the Easter', () => {
		expect(isEaster(2015, 3, 5)).toBe(false);
	});

	it('16.4.2015 is not the Easter', () => {
		expect(isEaster(2015, 4, 16)).toBe(false);
	});

	it('11.5.2015 is not the Easter', () => {
		expect(isEaster(2015, 5, 11)).toBe(false);
	});

	it('29.9.2015 is not the Easter', () => {
		expect(isEaster(2015, 9, 29)).toBe(false);
	});

	it('17.10.2015 is not the Easter', () => {
		expect(isEaster(2015, 10, 17)).toBe(false);
	});

	it('30.10.2015 is not the Easter', () => {
		expect(isEaster(2015, 10, 30)).toBe(false);
	});

	it('24.11.2015 is not the Easter', () => {
		expect(isEaster(2015, 11, 24)).toBe(false);
	});

	it('16.1.2016 is not the Easter', () => {
		expect(isEaster(2016, 1, 16)).toBe(false);
	});

	it('25.2.2016 is not the Easter', () => {
		expect(isEaster(2016, 2, 25)).toBe(false);
	});

	it('8.6.2016 is not the Easter', () => {
		expect(isEaster(2016, 6, 8)).toBe(false);
	});

	it('5.8.2016 is not the Easter', () => {
		expect(isEaster(2016, 8, 5)).toBe(false);
	});

	it('30.10.2016 is not the Easter', () => {
		expect(isEaster(2016, 10, 30)).toBe(false);
	});

	it('10.11.2016 is not the Easter', () => {
		expect(isEaster(2016, 11, 10)).toBe(false);
	});

	it('1.2.2017 is not the Easter', () => {
		expect(isEaster(2017, 2, 1)).toBe(false);
	});

	it('16.3.2017 is not the Easter', () => {
		expect(isEaster(2017, 3, 16)).toBe(false);
	});

	it('17.5.2017 is not the Easter', () => {
		expect(isEaster(2017, 5, 17)).toBe(false);
	});

	it('29.6.2017 is not the Easter', () => {
		expect(isEaster(2017, 6, 29)).toBe(false);
	});

	it('1.10.2017 is not the Easter', () => {
		expect(isEaster(2017, 10, 1)).toBe(false);
	});

	it('30.10.2017 is not the Easter', () => {
		expect(isEaster(2017, 10, 30)).toBe(false);
	});

	it('21.12.2017 is not the Easter', () => {
		expect(isEaster(2017, 12, 21)).toBe(false);
	});

	it('9.1.2018 is not the Easter', () => {
		expect(isEaster(2018, 1, 9)).toBe(false);
	});

	it('30.4.2018 is not the Easter', () => {
		expect(isEaster(2018, 4, 30)).toBe(false);
	});

	it('2.5.2018 is not the Easter', () => {
		expect(isEaster(2018, 5, 2)).toBe(false);
	});

	it('15.6.2018 is not the Easter', () => {
		expect(isEaster(2018, 6, 15)).toBe(false);
	});

	it('21.9.2018 is not the Easter', () => {
		expect(isEaster(2018, 9, 21)).toBe(false);
	});

	it('17.12.2018 is not the Easter', () => {
		expect(isEaster(2018, 12, 17)).toBe(false);
	});

	it('6.2.2019 is not the Easter', () => {
		expect(isEaster(2019, 2, 6)).toBe(false);
	});

	it('19.3.2019 is not the Easter', () => {
		expect(isEaster(2019, 3, 19)).toBe(false);
	});

	it('21.5.2019 is not the Easter', () => {
		expect(isEaster(2019, 5, 21)).toBe(false);
	});

	it('15.6.2019 is not the Easter', () => {
		expect(isEaster(2019, 6, 15)).toBe(false);
	});

	it('2.8.2019 is not the Easter', () => {
		expect(isEaster(2019, 8, 2)).toBe(false);
	});

	it('12.9.2019 is not the Easter', () => {
		expect(isEaster(2019, 9, 12)).toBe(false);
	});

	it('30.10.2019 is not the Easter', () => {
		expect(isEaster(2019, 10, 30)).toBe(false);
	});

	it('10.1.2020 is not the Easter', () => {
		expect(isEaster(2020, 1, 10)).toBe(false);
	});

	it('16.1.2020 is not the Easter', () => {
		expect(isEaster(2020, 1, 16)).toBe(false);
	});

	it('10.2.2020 is not the Easter', () => {
		expect(isEaster(2020, 2, 10)).toBe(false);
	});

	it('12.3.2020 is not the Easter', () => {
		expect(isEaster(2020, 3, 12)).toBe(false);
	});

	it('28.5.2020 is not the Easter', () => {
		expect(isEaster(2020, 5, 28)).toBe(false);
	});

	it('15.7.2020 is not the Easter', () => {
		expect(isEaster(2020, 7, 15)).toBe(false);
	});

	it('20.8.2020 is not the Easter', () => {
		expect(isEaster(2020, 8, 20)).toBe(false);
	});

	it('10.9.2020 is not the Easter', () => {
		expect(isEaster(2020, 9, 10)).toBe(false);
	});

	it('30.10.2020 is not the Easter', () => {
		expect(isEaster(2020, 10, 30)).toBe(false);
	});

	it('18.11.2020 is not the Easter', () => {
		expect(isEaster(2020, 11, 18)).toBe(false);
	});

	it('28.12.2020 is not the Easter', () => {
		expect(isEaster(2020, 12, 28)).toBe(false);
	});

	it('6.2.2021 is not the Easter', () => {
		expect(isEaster(2021, 2, 6)).toBe(false);
	});

	it('8.4.2021 is not the Easter', () => {
		expect(isEaster(2021, 4, 8)).toBe(false);
	});

	it('5.8.2021 is not the Easter', () => {
		expect(isEaster(2021, 8, 5)).toBe(false);
	});

	it('20.8.2021 is not the Easter', () => {
		expect(isEaster(2021, 8, 20)).toBe(false);
	});

	it('19.9.2021 is not the Easter', () => {
		expect(isEaster(2021, 9, 19)).toBe(false);
	});

	it('30.10.2021 is not the Easter', () => {
		expect(isEaster(2021, 10, 30)).toBe(false);
	});

	it('4.12.2021 is not the Easter', () => {
		expect(isEaster(2021, 12, 4)).toBe(false);
	});
});
