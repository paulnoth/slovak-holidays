const isSameMonth = require('../../src/util/isSameMonth');

describe('isSameMonth', () => {
	it('1 is on a same month as 1.1.1993', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isSameMonth(holiday, 1)).toBe(true);
	});

	it('2 is not on a same month as 1.1.1993', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isSameMonth(holiday, 2)).toBe(false);
    });
    
    it('null is not on a same month as 1.1.1993', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isSameMonth(holiday, null)).toBe(false);
    });
    
    it('undefined is not on a same month as 1.1.1993', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isSameMonth(holiday)).toBe(false);
	});
});