const isYearInRange = require('../../src/util/isYearInRange');

describe('isYearInRange', () => {
	it('1993 is in holiday year range for Independence day', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isYearInRange(holiday, 1993)).toBe(true);
	});

	it('1992 is not in holiday year range for Independence day', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isYearInRange(holiday, 1992)).toBe(false);
	});
    
    it('null is not in holiday year range for Independence day', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isYearInRange(holiday, null)).toBe(false);
	});
    
    it('undefined is not in holiday year range for Independence day', () => {
        const holiday = {
            "day": 1,
            "month": 1,
            "validFrom": 1993,
            "validTo": 9999
        };
		expect(isYearInRange(holiday)).toBe(false);
	});
});