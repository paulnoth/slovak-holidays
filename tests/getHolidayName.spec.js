const getHolidayName = require('../src/getHolidayName');

describe('getHolidayName', () => {

	// getHolidayName(date)

	it('1.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 0, 1))).toBe(null);
	});

	it('6.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 0, 6))).toBe(null);
	});

	it('9.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 9))).toBe(null);
	});

	it('11.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 9))).toBe(null);
	});

	it('12.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 12))).toBe(null);
	});

	it('1.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 4, 1))).toBe(null);
	});

	it('8.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 4, 8))).toBe(null);
	});

	it('5.7.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 6, 5))).toBe(null);
	});

	it('29.8.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 7, 29))).toBe(null);
	});

	it('1.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 8, 1))).toBe(null);
	});

	it('15.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 8, 15))).toBe(null);
	});

	it('1.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 10, 1))).toBe(null);
	});

	it('17.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 10, 17))).toBe(null);
	});

	it('24.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 24))).toBe(null);
	});

	it('25.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 25))).toBe(null);
	});

	it('26.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 26))).toBe(null);
	});

	it('1.1.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('9.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 9))).toBe('Veľký piatok');
	});

	it('11.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 11))).toBe('Veľkonočná nedeľa');
	});

	it('12.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 12))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1993, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('1.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 3, 1))).toBe('Veľký piatok');
	});

	it('3.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 3, 3))).toBe('Veľkonočná nedeľa');
	});

	it('4.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 3, 4))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1994, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 3, 14))).toBe('Veľký piatok');
	});

	it('16.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 3, 16))).toBe('Veľkonočná nedeľa');
	});

	it('17.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 3, 17))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1995, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('5.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 3, 5))).toBe('Veľký piatok');
	});

	it('7.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 3, 7))).toBe('Veľkonočná nedeľa');
	});

	it('8.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 3, 8))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1996, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('28.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 2, 28))).toBe('Veľký piatok');
	});

	it('30.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 2, 30))).toBe('Veľkonočná nedeľa');
	});

	it('31.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 2, 31))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1997, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 3, 10))).toBe('Veľký piatok');
	});

	it('12.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 3, 12))).toBe('Veľkonočná nedeľa');
	});

	it('13.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 3, 13))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1998, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 3, 2))).toBe('Veľký piatok');
	});

	it('4.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 3, 4))).toBe('Veľkonočná nedeľa');
	});

	it('5.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 3, 5))).toBe('Veľkonočný pondelok');
	});

	it('1.5.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(1999, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('21.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 3, 21))).toBe('Veľký piatok');
	});

	it('23.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 3, 23))).toBe('Veľkonočná nedeľa');
	});

	it('24.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 3, 24))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2000, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('13.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 3, 13))).toBe('Veľký piatok');
	});

	it('15.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 3, 15))).toBe('Veľkonočná nedeľa');
	});

	it('16.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 3, 16))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2001, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('29.3.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 2, 29))).toBe('Veľký piatok');
	});

	it('31.3.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 2, 31))).toBe('Veľkonočná nedeľa');
	});

	it('1.4.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 3, 1))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2002, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('18.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 3, 18))).toBe('Veľký piatok');
	});

	it('20.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 3, 20))).toBe('Veľkonočná nedeľa');
	});

	it('21.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 3, 21))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2003, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('9.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 3, 9))).toBe('Veľký piatok');
	});

	it('11.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 3, 11))).toBe('Veľkonočná nedeľa');
	});

	it('12.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 3, 12))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2004, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('25.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 2, 25))).toBe('Veľký piatok');
	});

	it('27.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 2, 27))).toBe('Veľkonočná nedeľa');
	});

	it('28.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 2, 28))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2005, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 3, 14))).toBe('Veľký piatok');
	});

	it('16.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 3, 16))).toBe('Veľkonočná nedeľa');
	});

	it('17.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 3, 17))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2006, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('6.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 3, 6))).toBe('Veľký piatok');
	});

	it('8.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 3, 8))).toBe('Veľkonočná nedeľa');
	});

	it('9.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 3, 9))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2007, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('21.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 2, 21))).toBe('Veľký piatok');
	});

	it('23.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 2, 23))).toBe('Veľkonočná nedeľa');
	});

	it('24.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 2, 24))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2008, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 3, 10))).toBe('Veľký piatok');
	});

	it('12.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 3, 12))).toBe('Veľkonočná nedeľa');
	});

	it('13.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 3, 13))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2009, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 3, 2))).toBe('Veľký piatok');
	});

	it('4.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 3, 4))).toBe('Veľkonočná nedeľa');
	});

	it('5.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 3, 5))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2010, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('22.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 3, 22))).toBe('Veľký piatok');
	});

	it('24.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 3, 24))).toBe('Veľkonočná nedeľa');
	});

	it('25.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 3, 25))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2011, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('6.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 3, 6))).toBe('Veľký piatok');
	});

	it('8.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 3, 8))).toBe('Veľkonočná nedeľa');
	});

	it('9.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 3, 9))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2012, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('29.3.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 2, 29))).toBe('Veľký piatok');
	});

	it('31.3.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 2, 31))).toBe('Veľkonočná nedeľa');
	});

	it('1.4.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 3, 1))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2013, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('18.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 3, 18))).toBe('Veľký piatok');
	});

	it('20.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 3, 20))).toBe('Veľkonočná nedeľa');
	});

	it('21.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 3, 21))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2014, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('3.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 3, 3))).toBe('Veľký piatok');
	});

	it('5.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 3, 5))).toBe('Veľkonočná nedeľa');
	});

	it('6.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 3, 6))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2015, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('25.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 2, 25))).toBe('Veľký piatok');
	});

	it('27.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 2, 27))).toBe('Veľkonočná nedeľa');
	});

	it('28.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 2, 28))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2016, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 3, 14))).toBe('Veľký piatok');
	});

	it('16.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 3, 16))).toBe('Veľkonočná nedeľa');
	});

	it('17.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 3, 17))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2017, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('30.3.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 2, 30))).toBe('Veľký piatok');
	});

	it('1.4.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 3, 1))).toBe('Veľkonočná nedeľa');
	});

	it('2.4.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 3, 2))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('30.10.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 9, 30))).toBe('Výročie Deklarácie slovenského národa');
	});

	it('1.11.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2018, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('19.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 3, 19))).toBe('Veľký piatok');
	});

	it('21.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 3, 21))).toBe('Veľkonočná nedeľa');
	});

	it('22.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 3, 22))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2019, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 3, 10))).toBe('Veľký piatok');
	});

	it('12.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 3, 12))).toBe('Veľkonočná nedeľa');
	});

	it('13.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 3, 13))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2020, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 3, 2))).toBe('Veľký piatok');
	});

	it('4.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 3, 4))).toBe('Veľkonočná nedeľa');
	});

	it('5.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 3, 5))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2021, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('15.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 3, 15))).toBe('Veľký piatok');
	});

	it('17.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 3, 17))).toBe('Veľkonočná nedeľa');
	});

	it('18.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 3, 18))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2022, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 0, 1))).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 0, 6))).toBe('Zjavenie Pána (Traja králi)');
	});

	it('7.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 3, 7))).toBe('Veľký piatok');
	});

	it('9.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 3, 9))).toBe('Veľkonočná nedeľa');
	});

	it('10.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 3, 10))).toBe('Veľkonočný pondelok');
	});

	it('1.5.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 4, 1))).toBe('Sviatok práce');
	});

	it('8.5.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 4, 8))).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 6, 5))).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 7, 29))).toBe('Výročie SNP');
	});

	it('1.9.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 8, 1))).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 8, 15))).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 10, 1))).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 10, 17))).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 11, 24))).toBe('Štedrý deň');
	});

	it('25.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 11, 25))).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName(new Date(2023, 11, 26))).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 0, 1))).toBe(null);
	});

	it('6.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 0, 6))).toBe(null);
	});

	it('9.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 9))).toBe(null);
	});

	it('11.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 11))).toBe(null);
	});

	it('12.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 3, 12))).toBe(null);
	});

	it('1.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 4, 1))).toBe(null);
	});

	it('8.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 4, 8))).toBe(null);
	});

	it('5.7.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 6, 5))).toBe(null);
	});

	it('29.8.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 7, 29))).toBe(null);
	});

	it('1.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 8, 1))).toBe(null);
	});

	it('15.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 8, 15))).toBe(null);
	});

	it('1.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 10, 1))).toBe(null);
	});

	it('17.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 10, 17))).toBe(null);
	});

	it('24.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 24))).toBe(null);
	});

	it('25.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 25))).toBe(null);
	});

	it('26.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1992, 11, 26))).toBe(null);
	});

	it('2.1.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 0, 2))).toBe(null);
	});

	it('5.1.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 0, 5))).toBe(null);
	});

	it('8.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 8))).toBe(null);
	});

	it('10.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 10))).toBe(null);
	});

	it('13.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 3, 13))).toBe(null);
	});

	it('2.5.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 4, 2))).toBe(null);
	});

	it('7.5.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 4, 7))).toBe(null);
	});

	it('3.7.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 6, 3))).toBe(null);
	});

	it('26.8.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 7, 26))).toBe(null);
	});

	it('11.9.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 8, 11))).toBe(null);
	});

	it('12.9.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 8, 12))).toBe(null);
	});

	it('30.10.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 9, 30))).toBe(null);
	});

	it('12.11.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 10, 12))).toBe(null);
	});

	it('19.11.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 10, 19))).toBe(null);
	});

	it('20.12.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1993, 11, 20))).toBe(null);
	});

	it('12.1.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 0, 12))).toBe(null);
	});

	it('8.1.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 0, 8))).toBe(null);
	});

	it('11.4.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 3, 11))).toBe(null);
	});

	it('30.4.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 3, 30))).toBe(null);
	});

	it('10.5.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 4, 10))).toBe(null);
	});

	it('18.5.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 4, 18))).toBe(null);
	});

	it('15.7.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 6, 15))).toBe(null);
	});

	it('21.8.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 7, 21))).toBe(null);
	});

	it('3.9.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 8, 3))).toBe(null);
	});

	it('10.9.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 8, 10))).toBe(null);
	});

	it('30.10.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 9, 30))).toBe(null);
	});

	it('13.11.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 10, 13))).toBe(null);
	});

	it('20.11.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1994, 10, 20))).toBe(null);
	});

	it('3.1.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 0, 3))).toBe(null);
	});

	it('10.1.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 0, 10))).toBe(null);
	});

	it('12.4.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 3, 12))).toBe(null);
	});

	it('3.5.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 4, 3))).toBe(null);
	});

	it('7.5.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 4, 7))).toBe(null);
	});

	it('6.7.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 6, 6))).toBe(null);
	});

	it('28.8.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 7, 28))).toBe(null);
	});

	it('2.9.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 8, 2))).toBe(null);
	});

	it('16.9.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 8, 16))).toBe(null);
	});

	it('30.10.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 9, 30))).toBe(null);
	});

	it('12.11.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 10, 12))).toBe(null);
	});

	it('28.12.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1995, 11, 28))).toBe(null);
	});

	it('2.1.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 0, 2))).toBe(null);
	});

	it('7.1.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 0, 7))).toBe(null);
	});

	it('3.4.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 3, 3))).toBe(null);
	});

	it('9.5.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 4, 9))).toBe(null);
	});

	it('4.7.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 6, 4))).toBe(null);
	});

	it('30.8.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 7, 30))).toBe(null);
	});

	it('14.9.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 8, 14))).toBe(null);
	});

	it('30.10.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 9, 30))).toBe(null);
	});

	it('2.11.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 10, 2))).toBe(null);
	});

	it('16.11.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 10, 16))).toBe(null);
	});

	it('31.12.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1996, 11, 31))).toBe(null);
	});

	it('2.1.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 0, 2))).toBe(null);
	});

	it('1.4.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 3, 1))).toBe(null);
	});

	it('5.5.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 4, 5))).toBe(null);
	});

	it('6.7.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 6, 6))).toBe(null);
	});

	it('27.8.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 7, 27))).toBe(null);
	});

	it('4.9.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 8, 4))).toBe(null);
	});

	it('12.9.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 8, 12))).toBe(null);
	});

	it('30.10.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 9, 30))).toBe(null);
	});

	it('16.11.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1997, 10, 16))).toBe(null);
	});

	it('10.3.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 2, 10))).toBe(null);
	});

	it('1.6.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 5, 1))).toBe(null);
	});

	it('27.8.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 7, 27))).toBe(null);
	});

	it('13.9.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 8, 13))).toBe(null);
	});

	it('30.10.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 9, 30))).toBe(null);
	});

	it('15.11.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1998, 10, 15))).toBe(null);
	});

	it('10.1.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 0, 10))).toBe(null);
	});

	it('20.4.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 3, 20))).toBe(null);
	});

	it('10.5.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 4, 10))).toBe(null);
	});

	it('15.7.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 6, 15))).toBe(null);
	});

	it('16.9.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 8, 16))).toBe(null);
	});

	it('30.10.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 9, 30))).toBe(null);
	});

	it('18.11.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 10, 18))).toBe(null);
	});

	it('23.12.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 11, 23))).toBe(null);
	});

	it('27.12.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(1999, 11, 27))).toBe(null);
	});

	it('12.1.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 0, 12))).toBe(null);
	});

	it('20.4.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 3, 20))).toBe(null);
	});

	it('22.4.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 3, 22))).toBe(null);
	});

	it('18.5.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 4, 18))).toBe(null);
	});

	it('15.7.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 6, 15))).toBe(null);
	});

	it('20.8.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 7, 20))).toBe(null);
	});

	it('11.9.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 8, 11))).toBe(null);
	});

	it('14.9.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 8, 14))).toBe(null);
	});

	it('30.10.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 9, 30))).toBe(null);
	});

	it('3.11.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 10, 3))).toBe(null);
	});

	it('16.11.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2000, 10, 16))).toBe(null);
	});

	it('12.1.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 0, 12))).toBe(null);
	});

	it('13.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 2, 13))).toBe(null);
	});

	it('15.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 2, 15))).toBe(null);
	});

	it('16.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 2, 16))).toBe(null);
	});

	it('1.7.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 6, 1))).toBe(null);
	});

	it('2.8.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 7, 2))).toBe(null);
	});

	it('1.10.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 9, 1))).toBe(null);
	});

	it('30.10.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 9, 30))).toBe(null);
	});

	it('15.11.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 10, 15))).toBe(null);
	});

	it('5.12.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2001, 11, 5))).toBe(null);
	});

	it('12.1.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 0, 12))).toBe(null);
	});

	it('28.3.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 2, 28))).toBe(null);
	});

	it('2.4.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 3, 2))).toBe(null);
	});

	it('2.5.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 4, 2))).toBe(null);
	});

	it('1.7.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 6, 1))).toBe(null);
	});

	it('25.8.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 7, 25))).toBe(null);
	});

	it('21.9.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 8, 21))).toBe(null);
	});

	it('30.10.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 9, 30))).toBe(null);
	});

	it('21.11.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2002, 10, 21))).toBe(null);
	});

	it('22.4.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 3, 22))).toBe(null);
	});

	it('5.5.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 4, 5))).toBe(null);
	});

	it('29.7.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 6, 29))).toBe(null);
	});

	it('1.8.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 7, 1))).toBe(null);
	});

	it('12.9.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 8, 12))).toBe(null);
	});

	it('1.12.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2003, 11, 1))).toBe(null);
	});

	it('11.1.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 0, 11))).toBe(null);
	});

	it('4.4.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 3, 4))).toBe(null);
	});

	it('6.5.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 4, 6))).toBe(null);
	});

	it('6.7.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 6, 6))).toBe(null);
	});

	it('10.9.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 8, 10))).toBe(null);
	});

	it('10.11.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2004, 10, 10))).toBe(null);
	});

	it('12.1.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 0, 12))).toBe(null);
	});

	it('31.3.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 2, 31))).toBe(null);
	});

	it('31.5.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 4, 31))).toBe(null);
	});

	it('31.7.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 6, 31))).toBe(null);
	});

	it('21.9.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 8, 21))).toBe(null);
	});

	it('12.11.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2005, 10, 12))).toBe(null);
	});

	it('16.1.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2006, 0, 16))).toBe(null);
	});

	it('14.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2006, 1, 14))).toBe(null);
	});

	it('16.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2006, 1, 16))).toBe(null);
	});

	it('17.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2006, 1, 17))).toBe(null);
	});

	it('20.6.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2006, 5, 20))).toBe(null);
	});

	it('9.3.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 2, 9))).toBe(null);
	});

	it('1.6.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 5, 1))).toBe(null);
	});

	it('8.6.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 5, 8))).toBe(null);
	});

	it('5.8.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 7, 5))).toBe(null);
	});

	it('1.10.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 9, 1))).toBe(null);
	});

	it('30.10.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 9, 30))).toBe(null);
	});

	it('17.12.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2007, 11, 17))).toBe(null);
	});

	it('7.1.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 0, 7))).toBe(null);
	});

	it('21.2.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 1, 21))).toBe(null);
	});

	it('25.3.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 2, 25))).toBe(null);
	});

	it('2.5.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 4, 2))).toBe(null);
	});

	it('5.6.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 5, 5))).toBe(null);
	});

	it('2.8.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 7, 2))).toBe(null);
	});

	it('2.9.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 8, 2))).toBe(null);
	});

	it('30.10.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 9, 30))).toBe(null);
	});

	it('12.11.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2008, 10, 12))).toBe(null);
	});

	it('13.1.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2009, 0, 13))).toBe(null);
	});

	it('1.6.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2009, 5, 1))).toBe(null);
	});

	it('8.7.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2009, 6, 8))).toBe(null);
	});

	it('25.9.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2009, 8, 25))).toBe(null);
	});

	it('28.12.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2009, 11, 28))).toBe(null);
	});

	it('1.2.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 1, 1))).toBe(null);
	});

	it('1.4.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 3, 1))).toBe(null);
	});

	it('18.5.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 4, 18))).toBe(null);
	});

	it('15.7.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 6, 15))).toBe(null);
	});

	it('2.8.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 7, 2))).toBe(null);
	});

	it('12.9.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 8, 12))).toBe(null);
	});

	it('25.9.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 8, 25))).toBe(null);
	});

	it('30.10.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 9, 30))).toBe(null);
	});

	it('14.11.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2010, 10, 14))).toBe(null);
	});

	it('2.1.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2011, 0, 2))).toBe(null);
	});

	it('26.4.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2011, 3, 26))).toBe(null);
	});

	it('12.5.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2011, 4, 12))).toBe(null);
	});

	it('18.5.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2011, 4, 18))).toBe(null);
	});

	it('20.12.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2011, 11, 20))).toBe(null);
	});

	it('16.1.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 0, 16))).toBe(null);
	});

	it('6.2.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 1, 6))).toBe(null);
	});

	it('8.3.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 2, 8))).toBe(null);
	});

	it('9.6.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 5, 9))).toBe(null);
	});

	it('19.8.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 7, 19))).toBe(null);
	});

	it('19.9.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2012, 8, 19))).toBe(null);
	});

	it('16.1.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 0, 16))).toBe(null);
	});

	it('28.2.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 1, 28))).toBe(null);
	});

	it('1.10.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 9, 1))).toBe(null);
	});

	it('30.10.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 9, 30))).toBe(null);
	});

	it('23.12.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 11, 23))).toBe(null);
	});

	it('27.12.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2013, 11, 27))).toBe(null);
	});

	it('6.2.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 1, 6))).toBe(null);
	});

	it('18.3.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 2, 18))).toBe(null);
	});

	it('8.6.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 5, 8))).toBe(null);
	});

	it('5.8.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 7, 5))).toBe(null);
	});

	it('15.10.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 9, 15))).toBe(null);
	});

	it('30.10.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 9, 30))).toBe(null);
	});

	it('1.12.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2014, 11, 1))).toBe(null);
	});

	it('7.1.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 0, 7))).toBe(null);
	});

	it('3.2.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 1, 3))).toBe(null);
	});

	it('5.3.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 2, 5))).toBe(null);
	});

	it('16.4.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 3, 16))).toBe(null);
	});

	it('11.5.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 4, 11))).toBe(null);
	});

	it('29.9.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 8, 29))).toBe(null);
	});

	it('17.10.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 9, 17))).toBe(null);
	});

	it('30.10.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 9, 30))).toBe(null);
	});

	it('24.11.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2015, 10, 24))).toBe(null);
	});

	it('16.1.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 0, 16))).toBe(null);
	});

	it('25.2.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 1, 25))).toBe(null);
	});

	it('8.6.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 5, 8))).toBe(null);
	});

	it('5.8.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 7, 5))).toBe(null);
	});

	it('30.10.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 9, 30))).toBe(null);
	});

	it('10.11.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2016, 10, 10))).toBe(null);
	});

	it('1.2.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 1, 1))).toBe(null);
	});

	it('16.3.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 2, 16))).toBe(null);
	});

	it('17.5.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 4, 17))).toBe(null);
	});

	it('29.6.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 5, 29))).toBe(null);
	});

	it('1.10.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 9, 1))).toBe(null);
	});

	it('30.10.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 9, 30))).toBe(null);
	});

	it('21.12.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2017, 11, 21))).toBe(null);
	});

	it('9.1.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 0, 9))).toBe(null);
	});

	it('30.4.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 3, 30))).toBe(null);
	});

	it('2.5.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 4, 2))).toBe(null);
	});

	it('15.6.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 5, 15))).toBe(null);
	});

	it('21.9.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 8, 21))).toBe(null);
	});

	it('17.12.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2018, 11, 17))).toBe(null);
	});

	it('6.2.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 1, 6))).toBe(null);
	});

	it('19.3.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 2, 19))).toBe(null);
	});

	it('21.5.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 4, 21))).toBe(null);
	});

	it('15.6.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 5, 15))).toBe(null);
	});

	it('2.8.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 7, 2))).toBe(null);
	});

	it('12.9.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 8, 12))).toBe(null);
	});

	it('30.10.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2019, 9, 30))).toBe(null);
	});

	it('10.1.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 0, 10))).toBe(null);
	});

	it('16.1.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 0, 16))).toBe(null);
	});

	it('10.2.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 1, 10))).toBe(null);
	});

	it('12.3.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 2, 12))).toBe(null);
	});

	it('28.5.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 4, 28))).toBe(null);
	});

	it('15.7.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 6, 15))).toBe(null);
	});

	it('20.8.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 7, 20))).toBe(null);
	});

	it('10.9.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 8, 10))).toBe(null);
	});

	it('30.10.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 9, 30))).toBe(null);
	});

	it('18.11.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 10, 18))).toBe(null);
	});

	it('28.12.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2020, 11, 28))).toBe(null);
	});

	it('6.2.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 1, 6))).toBe(null);
	});

	it('8.4.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 3, 8))).toBe(null);
	});

	it('5.8.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 7, 5))).toBe(null);
	});

	it('20.8.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 7, 20))).toBe(null);
	});

	it('19.9.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 8, 19))).toBe(null);
	});

	it('30.10.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 9, 30))).toBe(null);
	});

	it('4.12.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName(new Date(2021, 11, 4))).toBe(null);
	});

	// getHolidayName(obj)

	it('should return false when no input', () => {
		expect(getHolidayName()).toBe(null);
	});

	it('should return false when "null" input', () => {
		expect(getHolidayName(null)).toBe(null);
	});

	it('should return false when non-object input', () => {
		expect(getHolidayName("null")).toBe(null);
	});

	it('should return false when non-date input', () => {
		expect(getHolidayName(4)).toBe(null);
	});

	it('should return false when empty object input', () => {
		expect(getHolidayName({})).toBe(null);
	});

	it('should return false when holiday object input without year', () => {
		expect(getHolidayName({month: 1, day: 1})).toBe(null);
	});

	it('should return false when holiday object input without month', () => {
		expect(getHolidayName({year: 1, day: 1})).toBe(null);
	});

	it('should return false when holiday object input without day', () => {
		expect(getHolidayName({month: 1, year: 1})).toBe(null);
	});

	it('1.1.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('9.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 9})).toBe('Veľký piatok');
	});

	it('11.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 11})).toBe('Veľkonočná nedeľa');
	});

	it('12.4.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 12})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1993 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1993, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('1.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 4, day: 1})).toBe('Veľký piatok');
	});

	it('3.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 4, day: 3})).toBe('Veľkonočná nedeľa');
	});

	it('4.4.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 4, day: 4})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1994 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1994, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 4, day: 14})).toBe('Veľký piatok');
	});

	it('16.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 4, day: 16})).toBe('Veľkonočná nedeľa');
	});

	it('17.4.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 4, day: 17})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1995 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1995, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('5.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 4, day: 5})).toBe('Veľký piatok');
	});

	it('7.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 4, day: 7})).toBe('Veľkonočná nedeľa');
	});

	it('8.4.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 4, day: 8})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1996 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1996, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('28.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 3, day: 28})).toBe('Veľký piatok');
	});

	it('30.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 3, day: 30})).toBe('Veľkonočná nedeľa');
	});

	it('31.3.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 3, day: 31})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1997 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1997, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 4, day: 10})).toBe('Veľký piatok');
	});

	it('12.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 4, day: 12})).toBe('Veľkonočná nedeľa');
	});

	it('13.4.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 4, day: 13})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1998 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1998, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 4, day: 2})).toBe('Veľký piatok');
	});

	it('4.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 4, day: 4})).toBe('Veľkonočná nedeľa');
	});

	it('5.4.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 4, day: 5})).toBe('Veľkonočný pondelok');
	});

	it('1.5.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.1999 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 1999, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('21.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 4, day: 21})).toBe('Veľký piatok');
	});

	it('23.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 4, day: 23})).toBe('Veľkonočná nedeľa');
	});

	it('24.4.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 4, day: 24})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2000 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2000, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('13.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 4, day: 13})).toBe('Veľký piatok');
	});

	it('15.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 4, day: 15})).toBe('Veľkonočná nedeľa');
	});

	it('16.4.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 4, day: 16})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2001 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2001, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('29.3.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 3, day: 29})).toBe('Veľký piatok');
	});

	it('31.3.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 3, day: 31})).toBe('Veľkonočná nedeľa');
	});

	it('1.4.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 4, day: 1})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2002 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2002, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('18.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 4, day: 18})).toBe('Veľký piatok');
	});

	it('20.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 4, day: 20})).toBe('Veľkonočná nedeľa');
	});

	it('21.4.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 4, day: 21})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2003 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2003, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('9.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 4, day: 9})).toBe('Veľký piatok');
	});

	it('11.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 4, day: 11})).toBe('Veľkonočná nedeľa');
	});

	it('12.4.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 4, day: 12})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2004 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2004, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('25.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 3, day: 25})).toBe('Veľký piatok');
	});

	it('27.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 3, day: 27})).toBe('Veľkonočná nedeľa');
	});

	it('28.3.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 3, day: 28})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2005 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2005, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 4, day: 14})).toBe('Veľký piatok');
	});

	it('16.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 4, day: 16})).toBe('Veľkonočná nedeľa');
	});

	it('17.4.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 4, day: 17})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2006 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2006, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('6.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 4, day: 6})).toBe('Veľký piatok');
	});

	it('8.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 4, day: 8})).toBe('Veľkonočná nedeľa');
	});

	it('9.4.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 4, day: 9})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2007 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2007, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('21.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 3, day: 21})).toBe('Veľký piatok');
	});

	it('23.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 3, day: 23})).toBe('Veľkonočná nedeľa');
	});

	it('24.3.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 3, day: 24})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2008 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2008, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 4, day: 10})).toBe('Veľký piatok');
	});

	it('12.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 4, day: 12})).toBe('Veľkonočná nedeľa');
	});

	it('13.4.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 4, day: 13})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2009 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2009, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 4, day: 2})).toBe('Veľký piatok');
	});

	it('4.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 4, day: 4})).toBe('Veľkonočná nedeľa');
	});

	it('5.4.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 4, day: 5})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2010 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2010, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('22.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 4, day: 22})).toBe('Veľký piatok');
	});

	it('24.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 4, day: 24})).toBe('Veľkonočná nedeľa');
	});

	it('25.4.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 4, day: 25})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2011 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2011, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('6.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 4, day: 6})).toBe('Veľký piatok');
	});

	it('8.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 4, day: 8})).toBe('Veľkonočná nedeľa');
	});

	it('9.4.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 4, day: 9})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2012 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2012, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('29.3.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 3, day: 29})).toBe('Veľký piatok');
	});

	it('31.3.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 3, day: 31})).toBe('Veľkonočná nedeľa');
	});

	it('1.4.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 4, day: 1})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2013 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2013, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('18.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 4, day: 18})).toBe('Veľký piatok');
	});

	it('20.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 4, day: 20})).toBe('Veľkonočná nedeľa');
	});

	it('21.4.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 4, day: 21})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2014 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2014, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('3.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 4, day: 3})).toBe('Veľký piatok');
	});

	it('5.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 4, day: 5})).toBe('Veľkonočná nedeľa');
	});

	it('6.4.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 4, day: 6})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2015 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2015, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('25.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 3, day: 25})).toBe('Veľký piatok');
	});

	it('27.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 3, day: 27})).toBe('Veľkonočná nedeľa');
	});

	it('28.3.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 3, day: 28})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2016 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2016, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('14.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 4, day: 14})).toBe('Veľký piatok');
	});

	it('16.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 4, day: 16})).toBe('Veľkonočná nedeľa');
	});

	it('17.4.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 4, day: 17})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2017 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2017, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('30.3.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 3, day: 30})).toBe('Veľký piatok');
	});

	it('1.4.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 4, day: 1})).toBe('Veľkonočná nedeľa');
	});

	it('2.4.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 4, day: 2})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('30.10.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 10, day: 30})).toBe('Výročie Deklarácie slovenského národa');
	});

	it('1.11.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2018 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2018, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('19.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 4, day: 19})).toBe('Veľký piatok');
	});

	it('21.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 4, day: 21})).toBe('Veľkonočná nedeľa');
	});

	it('22.4.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 4, day: 22})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2019 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2019, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('10.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 4, day: 10})).toBe('Veľký piatok');
	});

	it('12.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 4, day: 12})).toBe('Veľkonočná nedeľa');
	});

	it('13.4.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 4, day: 13})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2020 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2020, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('2.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 4, day: 2})).toBe('Veľký piatok');
	});

	it('4.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 4, day: 4})).toBe('Veľkonočná nedeľa');
	});

	it('5.4.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 4, day: 5})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2021 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2021, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('15.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 4, day: 15})).toBe('Veľký piatok');
	});

	it('17.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 4, day: 17})).toBe('Veľkonočná nedeľa');
	});

	it('18.4.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 4, day: 18})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2022 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2022, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 1, day: 1})).toBe('Deň vzniku Slovenskej republiky');
	});

	it('6.1.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 1, day: 6})).toBe('Zjavenie Pána (Traja králi)');
	});

	it('7.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 4, day: 7})).toBe('Veľký piatok');
	});

	it('9.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 4, day: 9})).toBe('Veľkonočná nedeľa');
	});

	it('10.4.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 4, day: 10})).toBe('Veľkonočný pondelok');
	});

	it('1.5.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 5, day: 1})).toBe('Sviatok práce');
	});

	it('8.5.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 5, day: 8})).toBe('Deň víťazstva nad fašizmom');
	});

	it('5.7.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 7, day: 5})).toBe('Sviatok svätého Cyrila a Metoda');
	});

	it('29.8.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 8, day: 29})).toBe('Výročie SNP');
	});

	it('1.9.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 9, day: 1})).toBe('Deň Ústavy Slovenskej republiky');
	});

	it('15.9.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 9, day: 15})).toBe('Sedembolestná Panna Mária');
	});

	it('1.11.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 11, day: 1})).toBe('Sviatok všetkých svätých');
	});

	it('17.11.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 11, day: 17})).toBe('Deň boja za slobodu a demokraciu');
	});

	it('24.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 12, day: 24})).toBe('Štedrý deň');
	});

	it('25.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 12, day: 25})).toBe('Prvý sviatok vianočný');
	});

	it('26.12.2023 returns en English name, is a holiday', () => {
		expect(getHolidayName({year: 2023, month: 12, day: 26})).toBe('Druhý sviatok vianočný');
	});

	it('1.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 1, day: 1})).toBe(null);
	});

	it('6.1.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 1, day: 6})).toBe(null);
	});

	it('9.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 4, day: 9})).toBe(null);
	});

	it('11.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 4, day: 11})).toBe(null);
	});

	it('12.4.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 4, day: 12})).toBe(null);
	});

	it('1.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 5, day: 1})).toBe(null);
	});

	it('8.5.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 5, day: 8})).toBe(null);
	});

	it('5.7.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 7, day: 5})).toBe(null);
	});

	it('29.8.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 8, day: 29})).toBe(null);
	});

	it('1.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 9, day: 1})).toBe(null);
	});

	it('15.9.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 9, day: 15})).toBe(null);
	});

	it('1.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 11, day: 1})).toBe(null);
	});

	it('17.11.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 11, day: 17})).toBe(null);
	});

	it('24.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 12, day: 24})).toBe(null);
	});

	it('25.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 12, day: 25})).toBe(null);
	});

	it('26.12.1992 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1992, month: 12, day: 26})).toBe(null);
	});

	it('2.1.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 1, day: 2})).toBe(null);
	});

	it('5.1.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 1, day: 5})).toBe(null);
	});

	it('8.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 8})).toBe(null);
	});

	it('10.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 10})).toBe(null);
	});

	it('13.4.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 4, day: 13})).toBe(null);
	});

	it('2.5.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 5, day: 2})).toBe(null);
	});

	it('7.5.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 5, day: 7})).toBe(null);
	});

	it('3.7.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 7, day: 3})).toBe(null);
	});

	it('26.8.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 8, day: 26})).toBe(null);
	});

	it('11.9.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 9, day: 11})).toBe(null);
	});

	it('12.9.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 9, day: 12})).toBe(null);
	});

	it('30.10.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 10, day: 30})).toBe(null);
	});

	it('12.11.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 11, day: 12})).toBe(null);
	});

	it('19.11.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 11, day: 19})).toBe(null);
	});

	it('20.12.1993 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1993, month: 12, day: 20})).toBe(null);
	});

	it('12.1.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 1, day: 12})).toBe(null);
	});

	it('8.1.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 1, day: 8})).toBe(null);
	});

	it('11.4.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 4, day: 11})).toBe(null);
	});

	it('30.4.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 4, day: 30})).toBe(null);
	});

	it('10.5.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 5, day: 10})).toBe(null);
	});

	it('18.5.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 5, day: 18})).toBe(null);
	});

	it('15.7.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 7, day: 15})).toBe(null);
	});

	it('21.8.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 8, day: 21})).toBe(null);
	});

	it('3.9.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 9, day: 3})).toBe(null);
	});

	it('10.9.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 9, day: 10})).toBe(null);
	});

	it('30.10.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 10, day: 30})).toBe(null);
	});

	it('13.11.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 11, day: 13})).toBe(null);
	});

	it('20.11.1994 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1994, month: 11, day: 20})).toBe(null);
	});

	it('3.1.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 1, day: 3})).toBe(null);
	});

	it('10.1.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 1, day: 10})).toBe(null);
	});

	it('12.4.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 4, day: 12})).toBe(null);
	});

	it('3.5.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 5, day: 3})).toBe(null);
	});

	it('7.5.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 5, day: 7})).toBe(null);
	});

	it('6.7.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 7, day: 6})).toBe(null);
	});

	it('28.8.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 8, day: 28})).toBe(null);
	});

	it('2.9.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 9, day: 2})).toBe(null);
	});

	it('16.9.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 9, day: 16})).toBe(null);
	});

	it('30.10.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 10, day: 30})).toBe(null);
	});

	it('12.11.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 11, day: 12})).toBe(null);
	});

	it('28.12.1995 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1995, month: 12, day: 28})).toBe(null);
	});

	it('2.1.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 1, day: 2})).toBe(null);
	});

	it('7.1.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 1, day: 7})).toBe(null);
	});

	it('3.4.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 4, day: 3})).toBe(null);
	});

	it('9.5.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 5, day: 9})).toBe(null);
	});

	it('4.7.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 7, day: 4})).toBe(null);
	});

	it('30.8.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 8, day: 30})).toBe(null);
	});

	it('14.9.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 9, day: 14})).toBe(null);
	});

	it('30.10.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 10, day: 30})).toBe(null);
	});

	it('2.11.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 11, day: 2})).toBe(null);
	});

	it('16.11.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 11, day: 16})).toBe(null);
	});

	it('31.12.1996 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1996, month: 12, day: 31})).toBe(null);
	});

	it('2.1.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 1, day: 2})).toBe(null);
	});

	it('1.4.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 4, day: 1})).toBe(null);
	});

	it('5.5.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 5, day: 5})).toBe(null);
	});

	it('6.7.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 7, day: 6})).toBe(null);
	});

	it('27.8.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 8, day: 27})).toBe(null);
	});

	it('4.9.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 9, day: 4})).toBe(null);
	});

	it('12.9.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 9, day: 12})).toBe(null);
	});

	it('30.10.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 10, day: 30})).toBe(null);
	});

	it('16.11.1997 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1997, month: 11, day: 16})).toBe(null);
	});

	it('10.3.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 3, day: 10})).toBe(null);
	});

	it('1.6.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 6, day: 1})).toBe(null);
	});

	it('27.8.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 8, day: 27})).toBe(null);
	});

	it('13.9.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 9, day: 13})).toBe(null);
	});

	it('30.10.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 10, day: 30})).toBe(null);
	});

	it('15.11.1998 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1998, month: 11, day: 15})).toBe(null);
	});

	it('10.1.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 1, day: 10})).toBe(null);
	});

	it('20.4.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 4, day: 20})).toBe(null);
	});

	it('10.5.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 5, day: 10})).toBe(null);
	});

	it('15.7.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 7, day: 15})).toBe(null);
	});

	it('16.9.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 9, day: 16})).toBe(null);
	});

	it('30.10.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 10, day: 30})).toBe(null);
	});

	it('18.11.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 11, day: 18})).toBe(null);
	});

	it('23.12.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 12, day: 23})).toBe(null);
	});

	it('27.12.1999 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 1999, month: 12, day: 27})).toBe(null);
	});

	it('12.1.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 1, day: 12})).toBe(null);
	});

	it('20.4.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 4, day: 20})).toBe(null);
	});

	it('22.4.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 4, day: 22})).toBe(null);
	});

	it('18.5.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 5, day: 18})).toBe(null);
	});

	it('15.7.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 7, day: 15})).toBe(null);
	});

	it('20.8.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 8, day: 20})).toBe(null);
	});

	it('11.9.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 9, day: 11})).toBe(null);
	});

	it('14.9.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 9, day: 14})).toBe(null);
	});

	it('30.10.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 10, day: 30})).toBe(null);
	});

	it('3.11.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 11, day: 3})).toBe(null);
	});

	it('16.11.2000 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2000, month: 11, day: 16})).toBe(null);
	});

	it('12.1.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 1, day: 12})).toBe(null);
	});

	it('13.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 3, day: 13})).toBe(null);
	});

	it('15.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 3, day: 15})).toBe(null);
	});

	it('16.3.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 3, day: 16})).toBe(null);
	});

	it('1.7.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 7, day: 1})).toBe(null);
	});

	it('2.8.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 8, day: 2})).toBe(null);
	});

	it('1.10.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 10, day: 1})).toBe(null);
	});

	it('30.10.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 10, day: 30})).toBe(null);
	});

	it('15.11.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 11, day: 15})).toBe(null);
	});

	it('5.12.2001 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2001, month: 12, day: 5})).toBe(null);
	});

	it('12.1.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 1, day: 12})).toBe(null);
	});

	it('28.3.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 3, day: 28})).toBe(null);
	});

	it('2.4.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 4, day: 2})).toBe(null);
	});

	it('2.5.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 5, day: 2})).toBe(null);
	});

	it('1.7.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 7, day: 1})).toBe(null);
	});

	it('25.8.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 8, day: 25})).toBe(null);
	});

	it('21.9.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 9, day: 21})).toBe(null);
	});

	it('30.10.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 10, day: 30})).toBe(null);
	});

	it('21.11.2002 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2002, month: 11, day: 21})).toBe(null);
	});

	it('22.4.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 4, day: 22})).toBe(null);
	});

	it('5.5.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 5, day: 5})).toBe(null);
	});

	it('29.7.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 7, day: 29})).toBe(null);
	});

	it('1.8.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 8, day: 1})).toBe(null);
	});

	it('12.9.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 9, day: 12})).toBe(null);
	});

	it('1.12.2003 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2003, month: 12, day: 1})).toBe(null);
	});

	it('11.1.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 1, day: 11})).toBe(null);
	});

	it('4.4.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 4, day: 4})).toBe(null);
	});

	it('6.5.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 5, day: 6})).toBe(null);
	});

	it('6.7.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 7, day: 6})).toBe(null);
	});

	it('10.9.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 9, day: 10})).toBe(null);
	});

	it('10.11.2004 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2004, month: 11, day: 10})).toBe(null);
	});

	it('12.1.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 1, day: 12})).toBe(null);
	});

	it('31.3.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 3, day: 31})).toBe(null);
	});

	it('31.5.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 5, day: 31})).toBe(null);
	});

	it('31.7.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 7, day: 31})).toBe(null);
	});

	it('21.9.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 9, day: 21})).toBe(null);
	});

	it('12.11.2005 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2005, month: 11, day: 12})).toBe(null);
	});

	it('16.1.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2006, month: 1, day: 16})).toBe(null);
	});

	it('14.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2006, month: 2, day: 14})).toBe(null);
	});

	it('16.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2006, month: 2, day: 16})).toBe(null);
	});

	it('17.2.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2006, month: 2, day: 17})).toBe(null);
	});

	it('20.6.2006 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2006, month: 6, day: 20})).toBe(null);
	});

	it('9.3.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 3, day: 9})).toBe(null);
	});

	it('1.6.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 6, day: 1})).toBe(null);
	});

	it('8.6.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 6, day: 8})).toBe(null);
	});

	it('5.8.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 8, day: 5})).toBe(null);
	});

	it('1.10.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 10, day: 1})).toBe(null);
	});

	it('30.10.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 10, day: 30})).toBe(null);
	});

	it('17.12.2007 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2007, month: 12, day: 17})).toBe(null);
	});

	it('7.1.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 1, day: 7})).toBe(null);
	});

	it('21.2.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 2, day: 21})).toBe(null);
	});

	it('25.3.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 3, day: 25})).toBe(null);
	});

	it('2.5.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 5, day: 2})).toBe(null);
	});

	it('5.6.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 6, day: 5})).toBe(null);
	});

	it('2.8.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 8, day: 2})).toBe(null);
	});

	it('2.9.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 9, day: 2})).toBe(null);
	});

	it('30.10.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 10, day: 30})).toBe(null);
	});

	it('12.11.2008 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2008, month: 11, day: 12})).toBe(null);
	});

	it('13.1.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2009, month: 1, day: 13})).toBe(null);
	});

	it('1.6.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2009, month: 6, day: 1})).toBe(null);
	});

	it('8.7.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2009, month: 7, day: 8})).toBe(null);
	});

	it('25.9.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2009, month: 9, day: 25})).toBe(null);
	});

	it('28.12.2009 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2009, month: 12, day: 28})).toBe(null);
	});

	it('1.2.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 2, day: 1})).toBe(null);
	});

	it('1.4.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 4, day: 1})).toBe(null);
	});

	it('18.5.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 5, day: 18})).toBe(null);
	});

	it('15.7.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 7, day: 15})).toBe(null);
	});

	it('2.8.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 8, day: 2})).toBe(null);
	});

	it('12.9.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 9, day: 12})).toBe(null);
	});

	it('25.9.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 9, day: 25})).toBe(null);
	});

	it('30.10.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 10, day: 30})).toBe(null);
	});

	it('14.11.2010 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2010, month: 11, day: 14})).toBe(null);
	});

	it('2.1.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2011, month: 1, day: 2})).toBe(null);
	});

	it('26.4.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2011, month: 4, day: 26})).toBe(null);
	});

	it('12.5.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2011, month: 5, day: 12})).toBe(null);
	});

	it('18.5.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2011, month: 5, day: 18})).toBe(null);
	});

	it('20.12.2011 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2011, month: 12, day: 20})).toBe(null);
	});

	it('16.1.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 1, day: 16})).toBe(null);
	});

	it('6.2.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 2, day: 6})).toBe(null);
	});

	it('8.3.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 3, day: 8})).toBe(null);
	});

	it('9.6.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 6, day: 9})).toBe(null);
	});

	it('19.8.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 8, day: 19})).toBe(null);
	});

	it('19.9.2012 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2012, month: 9, day: 19})).toBe(null);
	});

	it('16.1.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 1, day: 16})).toBe(null);
	});

	it('28.2.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 2, day: 28})).toBe(null);
	});

	it('1.10.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 10, day: 1})).toBe(null);
	});

	it('30.10.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 10, day: 30})).toBe(null);
	});

	it('23.12.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 12, day: 23})).toBe(null);
	});

	it('27.12.2013 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2013, month: 12, day: 27})).toBe(null);
	});

	it('6.2.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 2, day: 6})).toBe(null);
	});

	it('18.3.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 3, day: 18})).toBe(null);
	});

	it('8.6.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 6, day: 8})).toBe(null);
	});

	it('5.8.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 8, day: 5})).toBe(null);
	});

	it('15.10.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 10, day: 15})).toBe(null);
	});

	it('30.10.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 10, day: 30})).toBe(null);
	});

	it('1.12.2014 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2014, month: 12, day: 1})).toBe(null);
	});

	it('7.1.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 1, day: 7})).toBe(null);
	});

	it('3.2.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 2, day: 3})).toBe(null);
	});

	it('5.3.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 3, day: 5})).toBe(null);
	});

	it('16.4.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 4, day: 16})).toBe(null);
	});

	it('11.5.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 5, day: 11})).toBe(null);
	});

	it('29.9.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 9, day: 29})).toBe(null);
	});

	it('17.10.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 10, day: 17})).toBe(null);
	});

	it('30.10.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 10, day: 30})).toBe(null);
	});

	it('24.11.2015 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2015, month: 11, day: 24})).toBe(null);
	});

	it('16.1.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 1, day: 16})).toBe(null);
	});

	it('25.2.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 2, day: 25})).toBe(null);
	});

	it('8.6.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 6, day: 8})).toBe(null);
	});

	it('5.8.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 8, day: 5})).toBe(null);
	});

	it('30.10.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 10, day: 30})).toBe(null);
	});

	it('10.11.2016 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2016, month: 11, day: 10})).toBe(null);
	});

	it('1.2.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 2, day: 1})).toBe(null);
	});

	it('16.3.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 3, day: 16})).toBe(null);
	});

	it('17.5.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 5, day: 17})).toBe(null);
	});

	it('29.6.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 6, day: 29})).toBe(null);
	});

	it('1.10.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 10, day: 1})).toBe(null);
	});

	it('30.10.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 10, day: 30})).toBe(null);
	});

	it('21.12.2017 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2017, month: 12, day: 21})).toBe(null);
	});

	it('9.1.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 1, day: 9})).toBe(null);
	});

	it('30.4.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 4, day: 30})).toBe(null);
	});

	it('2.5.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 5, day: 2})).toBe(null);
	});

	it('15.6.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 6, day: 15})).toBe(null);
	});

	it('21.9.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 9, day: 21})).toBe(null);
	});

	it('17.12.2018 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2018, month: 12, day: 17})).toBe(null);
	});

	it('6.2.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 2, day: 6})).toBe(null);
	});

	it('19.3.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 3, day: 19})).toBe(null);
	});

	it('21.5.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 5, day: 21})).toBe(null);
	});

	it('15.6.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 6, day: 15})).toBe(null);
	});

	it('2.8.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 8, day: 2})).toBe(null);
	});

	it('12.9.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 9, day: 12})).toBe(null);
	});

	it('30.10.2019 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2019, month: 10, day: 30})).toBe(null);
	});

	it('10.1.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 1, day: 10})).toBe(null);
	});

	it('16.1.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 1, day: 16})).toBe(null);
	});

	it('10.2.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 2, day: 10})).toBe(null);
	});

	it('12.3.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 3, day: 12})).toBe(null);
	});

	it('28.5.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 5, day: 28})).toBe(null);
	});

	it('15.7.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 7, day: 15})).toBe(null);
	});

	it('20.8.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 8, day: 20})).toBe(null);
	});

	it('10.9.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 9, day: 10})).toBe(null);
	});

	it('30.10.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 10, day: 30})).toBe(null);
	});

	it('18.11.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 11, day: 18})).toBe(null);
	});

	it('28.12.2020 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2020, month: 12, day: 28})).toBe(null);
	});

	it('6.2.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 2, day: 6})).toBe(null);
	});

	it('8.4.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 4, day: 8})).toBe(null);
	});

	it('5.8.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 8, day: 5})).toBe(null);
	});

	it('20.8.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 8, day: 20})).toBe(null);
	});

	it('19.9.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 9, day: 19})).toBe(null);
	});

	it('30.10.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 10, day: 30})).toBe(null);
	});

	it('4.12.2021 returns "null", is not a holiday', () => {
		expect(getHolidayName({year: 2021, month: 12, day: 4})).toBe(null);
	});
});
