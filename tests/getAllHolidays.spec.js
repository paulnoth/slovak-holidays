const getAllHolidays = require('../src/getAllHolidays');

const isHolidaySooner = (hol1, hol2) => {
    if(hol1.month > hol2.month) {
        return false;
    }
    if(hol1.month < hol2.month) {
        return true;
    }
    return hol1.day < hol2.day
}

describe('getAllHolidays', () => {
    it('should return empty array when no input parameter', () => {
        const result = getAllHolidays();
        expect(result.length).toBe(0);
    });

    it('should return empty array when "null" parameter', () => {
        const result = getAllHolidays(null);
        expect(result.length).toBe(0);
    });

    it('should return empty array when input is not a number', () => {
        const result = getAllHolidays("1993");
        expect(result.length).toBe(0);
    });

    it('should return empty array when "year" parameter is less then 1993', () => {
        const result = getAllHolidays(1992);
        expect(result.length).toBe(0);
    });

    it('should return 16 holidays for 1993', () => {
        const result = getAllHolidays(1993);
        expect(result.length).toBe(16);
    });

    it('should array of objects with following fields', () => {
        const holidays = getAllHolidays(2019);
        const containsAllFields = holidays.every(hol => {
            const fields = Object.keys(hol);
            const containsYear = fields.some(f => f === 'year');
            const containsMonth = fields.some(f => f === 'month');
            const containsDay = fields.some(f => f === 'day');
            const containsName = fields.some(f => f === 'name');
            return containsYear && containsMonth && containsDay && containsName;
        });
        expect(containsAllFields).toBe(true);
    });

    it('should return 16 holidays for 2019', () => {
        const result = getAllHolidays(2019);
        expect(result.length).toBe(16);
    });

    it('should return 17 holidays for 2018', () => {
        const result = getAllHolidays(2018);
        expect(result.length).toBe(17);
    });

    it('should return holidays chronologically for 2018', () => {
        const result = getAllHolidays(2018);

        let isChronological = true;
        for(let i = 0; i < result.length - 1; i++) {
            const hol1 = result[i];
            const hol2 = result[i + 1];
            isChronological = isChronological && isHolidaySooner(hol1, hol2);
        }
        expect(isChronological).toBe(true);
    });

    it('should return holidays chronologically 2019', () => {
        const result = getAllHolidays(2019);

        let isChronological = true;
        for(let i = 0; i < result.length - 1; i++) {
            const hol1 = result[i];
            const hol2 = result[i + 1];
            isChronological = isChronological && isHolidaySooner(hol1, hol2);
        }
        expect(isChronological).toBe(true);
    });
});